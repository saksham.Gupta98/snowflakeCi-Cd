import subprocess
import os
import sys
from os import walk
import shutil
import json
import smtplib
from email.message import EmailMessage
import pandas as pd
import snowflake.connector
from pandas import DataFrame

#testing git functions

# os.environ["SF_ACCOUNT"] = "elysiumpartner.snowflakecomputing.com"
os.environ["SF_USERNAME"] = "app"
# os.environ["SF_ROLE"] = "sysadmin"
# os.environ["SF_WAREHOUSE"] = "demo_wh"
# os.environ["SF_DATABASE"] = "tenant3"
os.environ["SNOWFLAKE_PASSWORD"] = "bigData123$"

print("sys argv value is:")
print(sys.argv[1])

if sys.argv[1] is not None and sys.argv[1] != "":
    print("from inputs")
    # print(sys.argv)
    m = sys.argv[1]
    t1 = eval(m)
    print(type(t1))
else:
    # Opening JSON file
    print("from release")
    vTagClient = sys.argv[2].split("_")[1:]
    print("vTagClient----",vTagClient)
    branch = str(sys.argv[3]).lower()
    print(branch)
    
    vTagClientStr = "_".join(vTagClient)
    print("vTagClientStr----",vTagClientStr)
    print(branch)

    if branch == "main":
        branch = "prod"
    elif branch == "qa":
        branch == "qa"
    elif branch == "dev":
        branch = "dev"
    

    print("branch---",branch)
    print(sys.argv[3])
    
    file = os.path.abspath('.') + "/tagJson/" + vTagClientStr + ".json"

    print(file)

    f = open(file)
      
    # returns JSON object as 
    # a dictionary
    t1 = json.load(f)

print(type(t1))
print(t1)
client_id = ""
account_name = ""
username = ""
role = ""
warehouse = ""
database = ""
schema = ""
component_names = []
versions_list = []
version = ""
password = ""

# getting the values from an api call
for i in range(0,len(t1)):
    print(type(t1[i]))
    for key,value in t1[i].items():
        if key.lower() == "role":
            role = role + value
        elif key.lower() == "client_id":
            client_id = client_id+value
        elif key.lower() == "warehouse":
            warehouse = warehouse+value
        elif key.lower() == "database":
            database = database+value
        elif key.lower() == "schema":
            schema = schema+value
        elif key.lower() == "parameters":
            parameters = value
        elif key.lower() == "component_name":
            print(type(value))
            print(value)
            for a in value:
                for g,h in a.items():
                     if g == 'component':
                        component_names.append(h)
                        versions_list.append(a['version'])
            # for i in range(0,len(value)):
            #     component_names.append(value[i])
        # elif key.lower() == "version":
        #     version = version+value
        elif key.lower() == "account_name":
            account_name = account_name+value
        elif key.lower() == "username":
            username = username+value
        elif key.lower() == "password":
            password = password+value
        else:
            pass

print("---listing out diretories ------")
print(component_names)
print(versions_list)

parameters_list = json.dumps(parameters)
print(parameters_list)
print(type(parameters_list))
# folder path
dir_path = './migrations'
# list to store files name
profile_files = []
alert_files = []
ingestion_summary = []
snowflake_audits_list = []
machine_learning_list = []
security_posture_list = []
alerts_1_list = []
alerts_2_list = []
directories = []
for (dir_path, dir_names, file_names) in walk(dir_path):
    directories.append(dir_path)    
    if dir_path == "./migrations/profile":
        profile_files.append(file_names)
       
    elif dir_path == "./migrations/alerts":
        alert_files.append(file_names)
    elif dir_path == "./migrations/ingestion_summary":
        ingestion_summary.append(file_names)
    elif dir_path == "./migrations/snowflake_audit":
        snowflake_audits_list.append(file_names)
    elif dir_path == "./migrations/machine_learning":
        machine_learning_list.append(file_names)
    elif dir_path == "./migrations/security_posture":
        security_posture_list.append(file_names)
    elif dir_path == "./migrations/alerts_1.x":
        alerts_1_list.append(file_names)
    elif dir_path == "./migrations/alerts_2.x":
        alerts_2_list.append(file_names)
    else:
        pass

def component_version_extraction(name):
    version = ""
    for i in range(0,len(t1)):
        print(type(t1[i]))
        for key,value in t1[i].items():
            if key.lower() == "component_name":
                for a in value:
                    for g,h in a.items():
                        if g == 'component':
                            if h.lower() == name.lower():
                                version =  version+a['version'] 
    return version       

# ----------versioning code to extract particular version files
def versioning_list(files,version):
    version_list = []
    print("inside method", files)
    if version.isnumeric() == True:
        version = int(version)  
        for i in range(1,version+1):
            l_in = [s for s in files if str(i) in s]
            if len(l_in) == 0:
                pass
            else:
                version_list.append(l_in)  
    else:
        print(version)
    print("in list method ",version_list)
    return version_list

# ---------- making components ready for deployement
def components_list(files,version,component_name):
    version_list = versioning_list(files,version)
    if len(version_list) == 0:
        pass
    else:
        print("inside profile", version_list)
        for i in range(0,len(version_list)):
            src = os.path.abspath('.')+"/migrations/"+component_name+"/"+version_list[i][0]   
            print(src)     
            dst = os.path.abspath('.')+"/migrations/"+component_name+"_temp"
            if os.path.exists(dst):
                pass
            else:
                os.mkdir(dst)
            shutil.copy(src, dst)
            
        return dst

def email_alert(username,password,account_name,role,warehouse,database,schema,profile_name):
    print("******* Email Part started ********")
    conn = snowflake.connector.connect(
                user=username,
                password=password,
                account=account_name,
                role=role,
                warehouse=warehouse,
                database=database,
                schema=schema                
                )
    sql_query = '''select Error_message from table(information_schema.query_history()) where user_name = 'APP' 
and query_tag like '%schemachange 3.4.1;%'and Execution_status like '%FAILED%'
and start_time >= DATEADD(minutes, -10, CURRENT_TIMESTAMP())'''
    query_result = pd.read_sql(sql_query,conn)
    empty_result = []
    if query_result.empty:
        df3 = profile_name+ " Component deployed successfully"
        empty_result.append(df3)
        df4 = DataFrame(empty_result,columns=['status'])
        agg_query_html = df4.to_html()
    else:
        print(query_result)
        agg_query_html = query_result.to_html()

    # Email Part
    
    EMAIL_ADDRESS = 'anithasingamsetti@gmail.com'
    EMAIL_PASSWORD = 'ukryefhdbvoiaoku'
    # contacts = "anitha.singamsetti@sstech.us,mythri.p@sstech.us"
    contacts = "anitha.singamsetti@elysiumanalytics.ai"

    msg = EmailMessage()
    msg['Subject'] = 'CI/CD Pipeline Component Deployement Status'
    msg['From'] = EMAIL_ADDRESS
    # msg['To'] = ', '.join(contacts)
    msg['To'] = contacts.split(',')

    msg.add_alternative(f"""\
    <!DOCTYPE html>
    <html>
        <body>
        <h4 style="color:BlueViolet;">Status of {profile_name} component </h4>{agg_query_html}
    </body>
    </html>
    """, subtype='html')

    with smtplib.SMTP_SSL('smtp.gmail.com',465) as smtp:
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        smtp.send_message(msg)

    print("email sent")        



for i in range(0,len(component_names)):
    if component_names[i].lower() == 'profile':
        version_num = component_version_extraction(name = component_names[i].lower())   
        dst = components_list(files=profile_files[0],version=version_num,component_name=component_names[i].lower())
        if dst == None:
            pass
        else:
            # print("schemachange", "-f",  dst, "-a", account_name , "-u", username,  "-r", role, "-w", warehouse, "-d", database, "-c", database+"."+schema+".PROFILE_CHANGE_HISTORY", "--vars", parameters_list, "--create-change-history-table")
            subprocess.run(["schemachange", "-f",  dst, "-a", account_name , "-u", username,  "-r", role, "-w", warehouse, "-d", database, "-c", database+"."+schema+".PROFILE_CHANGE_HISTORY", "--vars", parameters_list, "--create-change-history-table"], stderr=subprocess.PIPE, text=True)
            test_resp = email_alert(username,password,account_name,role,warehouse,database,schema,profile_name = 'profile')
            print(" file removed ", dst)
            shutil.rmtree(dst)

    if component_names[i].lower() == 'testCommit':
        version_num = component_version_extraction(name = component_names[i].lower())   
        dst = components_list(files=profile_files[0],version=version_num,component_name=component_names[i].lower())
        if dst == None:
            pass
        else:
            # print("schemachange", "-f",  dst, "-a", account_name , "-u", username,  "-r", role, "-w", warehouse, "-d", database, "-c", database+"."+schema+".PROFILE_CHANGE_HISTORY", "--vars", parameters_list, "--create-change-history-table")
            subprocess.run(["schemachange", "-f",  dst, "-a", account_name , "-u", username,  "-r", role, "-w", warehouse, "-d", database, "-c", database+"."+schema+".testCommit_CHANGE_HISTORY", "--vars", parameters_list, "--create-change-history-table"], stderr=subprocess.PIPE, text=True)
            test_resp = email_alert(username,password,account_name,role,warehouse,database,schema,profile_name = 'profile')
            print(" file removed ", dst)
            shutil.rmtree(dst)

    elif component_names[i].lower() == 'alerts':
        version_num = component_version_extraction(name = component_names[i].lower())
        dst = components_list(files=alert_files[0],version=version_num,component_name=component_names[i].lower())  
        if dst == None:
            pass
        else:
            subprocess.run(["schemachange", "-f",  dst , "-a", account_name , "-u", username,  "-r", role, "-w", warehouse, "-d", database, "-c", database+"."+schema+".ALERT_CHANGE_HISTORY", "--vars", parameters_list , "--create-change-history-table"], stderr=subprocess.PIPE, text=True)
            test_resp = email_alert(username,password,account_name,role,warehouse,database,schema,profile_name = 'alerts')
            print(" file removed ", dst)
            shutil.rmtree(dst)
    elif component_names[i].lower() == 'ingestion_summary':
        version_num = component_version_extraction(name = component_names[i].lower())
        dst = components_list(files=ingestion_summary[0],version=version_num,component_name=component_names[i].lower())  
        if dst == None:
            pass
        else:
            
            subprocess.run(["schemachange", "-f",  dst , "-a", account_name , "-u", username,  "-r", role, "-w", warehouse, "-d", database, "-c", database+"."+schema+".INGESTION_SUMMARY_CHANGE_HISTORY", "--vars", parameters_list ,"--create-change-history-table"], stderr=subprocess.PIPE, text=True)
            test_resp = email_alert(username,password,account_name,role,warehouse,database,schema,profile_name = 'ingestion_summary')
            print(" file removed ", dst)
            shutil.rmtree(dst)
    elif component_names[i].lower() == 'snowflake_audit':
        version_num = component_version_extraction(name = component_names[i].lower())
        dst = components_list(files=snowflake_audits_list[0],version=version_num,component_name=component_names[i].lower())  
        if dst == None:
            pass
        else:
            
            subprocess.run(["schemachange", "-f",  dst , "-a", account_name , "-u", username,  "-r", role, "-w", warehouse, "-d", database, "-c", database+"."+schema+".SNOWFLAKE_AUDIT_CHANGE_HISTORY", "--vars", parameters_list ,"--create-change-history-table", "--query-tag", "audit"], stderr=subprocess.PIPE, text=True)
            test_resp = email_alert(username,password,account_name,role,warehouse,database,schema,profile_name = 'snowflake_audit')
            # print(test_resp)
            print(" file removed ", dst)
            shutil.rmtree(dst)
    elif component_names[i].lower() == 'machine_learning':
        version_num = component_version_extraction(name = component_names[i].lower())
        dst = components_list(files=machine_learning_list[0],version=version_num,component_name=component_names[i].lower())  
        if dst == None:
            pass
        else:
            subprocess.run(["schemachange", "-f",  dst , "-a", account_name , "-u", username,  "-r", role, "-w", warehouse, "-d", database, "-c", database+"."+schema+".MACHINE_LEARNING_CHANGE_HISTORY", "--vars", parameters_list , "--create-change-history-table"], stderr=subprocess.PIPE, text=True)
            test_resp = email_alert(username,password,account_name,role,warehouse,database,schema,profile_name = 'alerts')
            print(" file removed ", dst)
            shutil.rmtree(dst)
    elif component_names[i].lower() == 'security_posture':
        version_num = component_version_extraction(name = component_names[i].lower())
        dst = components_list(files=security_posture_list[0],version=version_num,component_name=component_names[i].lower())  
        if dst == None:
            pass
        else:
            subprocess.run(["schemachange", "-f",  dst , "-a", account_name , "-u", username,  "-r", role, "-w", warehouse, "-d", database, "-c", database+"."+schema+".SECURITY_POSTURE_CHANGE_HISTORY", "--vars", parameters_list , "--create-change-history-table"], stderr=subprocess.PIPE, text=True)
            test_resp = email_alert(username,password,account_name,role,warehouse,database,schema,profile_name = 'alerts')
            print(" file removed ", dst)
            shutil.rmtree(dst)
    elif component_names[i].lower() == 'alerts_1.x':
        version_num = component_version_extraction(name = component_names[i].lower())
        dst = components_list(files=alerts_1_list[0],version=version_num,component_name=component_names[i].lower())  
        if dst == None:
            pass
        else:
            subprocess.run(["schemachange", "-f",  dst , "-a", account_name , "-u", username,  "-r", role, "-w", warehouse, "-d", database, "-c", database+"."+schema+".ALERTS_1.x_CHANGE_HISTORY", "--vars", parameters_list , "--create-change-history-table"], stderr=subprocess.PIPE, text=True)
            test_resp = email_alert(username,password,account_name,role,warehouse,database,schema,profile_name = 'alerts')
            print(" file removed ", dst)
            shutil.rmtree(dst)
    elif component_names[i].lower() == 'alerts_2.x':
        version_num = component_version_extraction(name = component_names[i].lower())
        dst = components_list(files=alerts_2_list[0],version=version_num,component_name=component_names[i].lower())  
        if dst == None:
            pass
        else:
            subprocess.run(["schemachange", "-f",  dst , "-a", account_name , "-u", username,  "-r", role, "-w", warehouse, "-d", database, "-c", database+"."+schema+".ALERTS_2.x_CHANGE_HISTORY", "--vars", parameters_list , "--create-change-history-table"], stderr=subprocess.PIPE, text=True)
            test_resp = email_alert(username,password,account_name,role,warehouse,database,schema,profile_name = 'alerts')
            print(" file removed ", dst)
            shutil.rmtree(dst)
    else:
        pass  

# -------------command to deploy all the componnets       



