CREATE OR REPLACE FUNCTION "DATE_DIFF_WORDS"("START_TIME" VARCHAR(16777216), "END_TIME" VARCHAR(16777216))
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
AS '
      
      var diff = new Date(END_TIME).getTime() - new Date(START_TIME).getTime();
      
      if (diff < 0) 
      {
        return ''Invalid Entry'';
      }
      else 
      {
        var str = '''';

        var days = Math.floor((((diff / 1000) / 60) / 60) / 24);
        var hours = Math.floor((((diff - days*24*60*60*1000) / 1000) / 60) / 60);
        var minutes = Math.floor(((diff - days*24*60*60*1000 - hours*60*60*1000) / 1000) / 60);
        var seconds = Math.floor((diff - days*24*60*60*1000 - hours*60*60*1000 - minutes*60*1000) / 1000);

        if (days == 1) {
        str = str + days + '' day '';
        }
        else if (days >= 1) {
        str = str + days + '' days '';
        }

        if (hours == 1) {
        str = str + hours + '' hr '';
        }
        else if (hours >= 1) {
        str = str + hours + '' hrs '';
        }

        if (minutes == 1) {
        str = str + minutes + '' min '';
        }
        else if (minutes >= 1) {
        str = str + minutes + '' mins '';
        }

        if (seconds == 1) {
        str = str + seconds + '' sec '';
        }
        else if (seconds >= 1) {
        str = str + seconds + '' secs '';
        }

        return str;
      }
      return str;
      ';
	  
	  
CREATE OR REPLACE PROCEDURE "INGEST_SUMMARY_LOAD"()
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS '
try
{

    snowflake.execute( {sqlText:`BEGIN`});
    
      
     //GET THE SQLS FOR SOURCE AND FREQUENCY
	var source_resultset = snowflake.execute({sqlText: `Select DISTINCT SOURCE FROM SOURCE_INGEST_SUMMARY_LKUP
    WHERE INGEST_IND = $$Y$$
    `});
    
    snowflake.execute({sqlText: `
    create OR REPLACE temp table INGEST_TEMP_TABLE 
    (
    DATE_TIME TIMESTAMP_NTZ(9),
    INGESTED_COUNT numeric,
    SOURCE string,
    COMMIT_TIME  TIMESTAMP_NTZ(9),
     TIME_DIFFERENCE   string
    )
    `});
      
    
	while (source_resultset.next())
    {  
    var table_nm = source_resultset.getColumnValue(1);
      
   snowflake.execute({sqlText: `
  INSERT INTO INGEST_TEMP_TABLE 
   select z.DATE_TIME ,
    z.INGESTED_COUNT, 
    z.SOURCE,
    MAX_COMMIT_TIME AS COMMIT_TIME,
    CASE WHEN z.MAX_COMMIT_TIME = $$1900-01-01 00:00:00.000$$ THEN NULL ELSE 
    DATE_DIFF_WORDS(to_varchar(z.MAX_COMMIT_TIME, $$YYYY-MM-DD HH24:MI:SS$$), 
                    to_varchar(convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9), $$YYYY-MM-DD HH24:MI:SS$$)) 
    END as TIME_DIFFERENCE
    from (
          WITH EVT_TBL AS
          (
          select
            time_slice(time,1,$$HOUR$$,$$START$$) AS start_time,
            sum(case when type = $$event$$ then 1 else 0 end) as ingested_count
            from
          (
          select $$event$$ as type,event_time::TIMESTAMP_NTZ(9) as time
            from `+ table_nm + ` where 
            event_time::TIMESTAMP_NTZ(9) > time_slice(convert_timezone($$EST5EDT$$,DATEADD( hour,-1,CURRENT_TIMESTAMP))::TIMESTAMP_NTZ(9),1, $$HOUR$$)
            and event_time::TIMESTAMP_NTZ(9)<=  time_slice(convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9),1, $$HOUR$$)
          )
          group by start_time
          ) ,
       DATE_GEN AS
      (
      select time_slice(convert_timezone($$EST5EDT$$,DATEADD( hour,-1,CURRENT_TIMESTAMP))::TIMESTAMP_NTZ(9),1, $$HOUR$$) AS date_time
 
      )
            ,  MAX_TM AS
          (
          select
    coalesce (MAX(COMMIT_TIME),$$1900-01-01$$::TIMESTAMP_NTZ(9)) as MAX_COMMIT_TIME
     from `+ table_nm + ` where 
       COMMIT_TIME::TIMESTAMP_NTZ(9) > time_slice(convert_timezone($$EST5EDT$$,DATEADD( hour,-1,CURRENT_TIMESTAMP))::TIMESTAMP_NTZ(9),1, $$HOUR$$)
       and COMMIT_TIME::TIMESTAMP_NTZ(9)<=  time_slice(convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9),1, $$HOUR$$)     
        )    
          select d.date_time, 
      IFNULL(e.ingested_count,0) AS ingested_count, 
     $$`+ table_nm + `$$ AS source
       ,F.MAX_COMMIT_TIME::TIMESTAMP_NTZ(9) as MAX_COMMIT_TIME
       from date_gen d
      left join evt_tbl e
      on d.date_time = e.start_time
        JOIN MAX_TM F 
    )z   
 `});      
      
 }     
      
  var stload_tbl = snowflake.execute( {sqlText:`
            SELECT SOURCE FROM (
            SELECT SOURCE,MAX(COMMIT_TIME) AS COMMIT_TIME  FROM 
            (SELECT SOURCE,COMMIT_TIME FROM INGEST_SUMMARY WHERE COMMIT_TIME  <> $$1900-01-01 00:00:00.000$$
             UNION ALL
             SELECT SOURCE,COMMIT_TIME FROM INGEST_TEMP_TABLE
            )
            INGEST_SUMMARY
            GROUP BY 1
            HAVING COUNT(*) = 1) A
            WHERE COMMIT_TIME  = $$1900-01-01 00:00:00.000$$
             `});     
            
      while (stload_tbl.next())
    { 
    stload_tbl_value = stload_tbl.getColumnValue(1);
            
         snowflake.execute( {sqlText:` update INGEST_TEMP_TABLE t1 set COMMIT_TIME = t2.Commit_time from     
            (select max(Commit_time) as Commit_time from `+ stload_tbl_value + `
          where COMMIT_TIME::TIMESTAMP_NTZ(9)<=  time_slice(convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9),1, $$HOUR$$)
          ) t2
            where t1.SOURCE = $$`+ stload_tbl_value + `$$
            `});  
    }        
            
          snowflake.execute( {sqlText:` update INGEST_TEMP_TABLE t1 set COMMIT_TIME = t2.Commit_time from     
            (
            select SOURCE,max(Commit_time) as Commit_time from INGEST_SUMMARY
            group by 1
          ) t2
            where t1.SOURCE = t2.source
            and t1.COMMIT_TIME =  $$1900-01-01 00:00:00.000$$
            `});  
            
            snowflake.execute( {sqlText:` update INGEST_TEMP_TABLE  set TIME_DIFFERENCE = 
               DATE_DIFF_WORDS(to_varchar(COMMIT_TIME, $$YYYY-MM-DD HH24:MI:SS$$), 
                    to_varchar(convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9), $$YYYY-MM-DD HH24:MI:SS$$))
            where TIME_DIFFERENCE is null
            `});              
            
            snowflake.execute({sqlText: `
delete from INGEST_SUMMARY where DATE_TIME =
time_slice(convert_timezone($$EST5EDT$$,DATEADD( hour,-1,CURRENT_TIMESTAMP))::TIMESTAMP_NTZ(9),1, $$HOUR$$)
    `});
    
            snowflake.execute({sqlText: `
UPDATE INGEST_SUMMARY SET CURR_IND = $$N$$
    `});        
        
    snowflake.execute( {sqlText:`INSERT INTO INGEST_SUMMARY (DATE_TIME,INGESTED_COUNT,SOURCE,COMMIT_TIME,TIME_DIFFERENCE,REFRESH_TIME,CURR_IND)
            SELECT DATE_TIME,INGESTED_COUNT,SOURCE,COMMIT_TIME,TIME_DIFFERENCE,convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9),$$Y$$ 
            FROM INGEST_TEMP_TABLE
            `});      
            

              
            
            	snowflake.execute( {sqlText:`COMMIT`});
}
catch(err)
{
	snowflake.execute({sqlText: "ROLLBACK"});
	return err;
}
return "Succeeded";
';   
	  
	  