
from rest_framework.views import APIView
from django.http import JsonResponse
import os
from inserting_data.libs.add_data_logic import snowflake_row_logic
from inserting_data.libs.check_id_logic import id_logic

class check_id(APIView):
    def post(self, request):
        json_input = request.data
        print(json_input)
        try:
            conn = id_logic()
            test = conn.main(json_input)
            return JsonResponse ({'success': True, 'message':test})
            
        except Exception as e:
            print(str(e))
            return JsonResponse({'success':False, 'message': str(e)}, status = 500)