use warehouse {{ warehouse }};
use database  {{ database }};

USE ROLE ACCOUNTADMIN;

grant imported privileges on database snowflake to role SYSADMIN;

USE ROLE SYSADMIN;	
CREATE SCHEMA IF NOT EXISTS SNOWFLAKEAUDIT;
USE SCHEMA SNOWFLAKEAUDIT;


CREATE TABLE IF NOT EXISTS AUDIT_LOG (
	QUERY_ID VARCHAR(16777216),
	QUERY_TEXT VARCHAR(16777216),
	DATABASE_NAME VARCHAR(16777216),
	SCHEMA_NAME VARCHAR(16777216),
	QUERY_TYPE VARCHAR(16777216),
	SESSION_ID NUMBER(38,0),
	USER_NAME VARCHAR(16777216),
	ROLE_NAME VARCHAR(16777216),
	WAREHOUSE_NAME VARCHAR(16777216),
	WAREHOUSE_SIZE VARCHAR(16777216),
	WAREHOUSE_TYPE VARCHAR(16777216),
	CLUSTER_NUMBER NUMBER(38,0),
	QUERY_TAG VARCHAR(16777216),
	EXECUTION_STATUS VARCHAR(16777216),
	ERROR_CODE NUMBER(38,0),
	ERROR_MESSAGE VARCHAR(16777216),
	START_TIME TIMESTAMP_LTZ(3),
	END_TIME TIMESTAMP_LTZ(3),
	TOTAL_ELAPSED_TIME NUMBER(38,0),
	BYTES_SCANNED NUMBER(38,0),
	ROWS_PRODUCED NUMBER(38,0),
	COMPILATION_TIME NUMBER(38,0),
	EXECUTION_TIME NUMBER(38,0),
	QUEUED_PROVISIONING_TIME NUMBER(38,0),
	QUEUED_REPAIR_TIME NUMBER(38,0),
	QUEUED_OVERLOAD_TIME NUMBER(38,0),
	TRANSACTION_BLOCKED_TIME NUMBER(38,0),
	OUTBOUND_DATA_TRANSFER_CLOUD VARCHAR(16777216),
	OUTBOUND_DATA_TRANSFER_REGION VARCHAR(16777216),
	OUTBOUND_DATA_TRANSFER_BYTES NUMBER(38,0),
	INBOUND_DATA_TRANSFER_CLOUD VARCHAR(16777216),
	INBOUND_DATA_TRANSFER_REGION VARCHAR(16777216),
	INBOUND_DATA_TRANSFER_BYTES NUMBER(38,0),
	CREDITS_USED_CLOUD_SERVICES NUMBER(38,9),
	LIST_EXTERNAL_FILE_TIME NUMBER(38,0),
	RELEASE_VERSION VARCHAR(16777216),
	EXTERNAL_FUNCTION_TOTAL_INVOCATIONS NUMBER(38,0),
	EXTERNAL_FUNCTION_TOTAL_SENT_ROWS NUMBER(38,0),
	EXTERNAL_FUNCTION_TOTAL_RECEIVED_ROWS NUMBER(38,0),
	EXTERNAL_FUNCTION_TOTAL_SENT_BYTES NUMBER(38,0),
	EXTERNAL_FUNCTION_TOTAL_RECEIVED_BYTES NUMBER(38,0),
	IS_CLIENT_GENERATED_STATEMENT BOOLEAN,
	INSERT_TIME TIMESTAMP_NTZ(9),
	TABLE_LIST VARIANT
);



CREATE  TABLE  IF NOT EXISTS ERROR (
	ERRDATE TIMESTAMP_NTZ(9),
	CODE VARCHAR(30),
	STATE VARCHAR(30),
	MESSAGE VARCHAR(16777216),
	STACKTRACETXT VARCHAR(16777216),
	SOURCETYPE VARCHAR(100),
	FILENAME VARCHAR(16777216),
	STAGE VARCHAR(16777216),
	INSERT_TIME TIMESTAMP_NTZ(9),
	GUID NUMBER(38,0)
);



CREATE TABLE IF NOT EXISTS LOGIN_HISTORY_LOG (
	EVENT_TIMESTAMP TIMESTAMP_LTZ(3),
	EVENT_ID NUMBER(38,0),
	EVENT_TYPE VARCHAR(16777216),
	USER_NAME VARCHAR(16777216),
	CLIENT_IP VARCHAR(16777216),
	REPORTED_CLIENT_TYPE VARCHAR(16777216),
	REPORTED_CLIENT_VERSION VARCHAR(16777216),
	FIRST_AUTHENTICATION_FACTOR VARCHAR(16777216),
	SECOND_AUTHENTICATION_FACTOR VARCHAR(16777216),
	IS_SUCCESS VARCHAR(3),
	ERROR_CODE NUMBER(38,0),
	ERROR_MESSAGE VARCHAR(16777216),
	RELATED_EVENT_ID NUMBER(38,0),
	INSERT_TIME TIMESTAMP_NTZ(9),
	ENRICHMENT_JSON VARIANT
);


CREATE  TABLE IF NOT EXISTS PIPE_DAILY_ACTIVITY_LOG (
	REF_DATE DATE,
	PIPE_ID NUMBER(38,0),
	PIPE_NAME VARCHAR(16777216),
	PIPE_SCHEMA VARCHAR(16777216),
	PIPE_CATALOG VARCHAR(16777216),
	PIPE_OWNER VARCHAR(16777216),
	CREATED TIMESTAMP_LTZ(9),
	LAST_DATA_PROCESS_DATE DATE,
	DAYS_SINCE_LAST_DATA_PROCESS NUMBER(38,0),
	TARGET_TABLE VARCHAR(16777216),
	INSERT_TIME TIMESTAMP_NTZ(9),
	DAILY_CREDITS_USED NUMBER(10,5),
	DAILY_BYTES_INSERTED NUMBER(38,0),
	DAILY_FILES_INSERTED NUMBER(38,0),
	DAILY_PIPES_EXE_COUNT NUMBER(38,0),
	DAILY_PIPES_EXE_TIME NUMBER(38,0)
);



CREATE  TABLE IF NOT EXISTS TABLE_STORAGE_ARCHIVE (
	ID NUMBER(9,0),
	USAGE_DATE TIMESTAMP_NTZ(9),
	TABLE_CREATED TIMESTAMP_LTZ(9),
	TABLE_CATALOG VARCHAR(16777216),
	TABLE_SCHEMA VARCHAR(16777216),
	TABLE_NAME VARCHAR(16777216),
	ACTIVE_BYTES NUMBER(38,0),
	TIME_TRAVEL_BYTES NUMBER(38,0),
	FAILSAFE_BYTES NUMBER(38,0),
	RETAINED_FOR_CLONE_BYTES NUMBER(38,0),
	STORAGE_BYTES NUMBER(38,0),
	INSERT_TIME TIMESTAMP_NTZ(9)
);


CREATE  TABLE  IF NOT EXISTS TASK_HISTORY_LOG (
	NAME VARCHAR(16777216),
	QUERY_TEXT VARCHAR(16777216),
	CONDITION_TEXT VARCHAR(16777216),
	SCHEMA_NAME VARCHAR(16777216),
	TASK_SCHEMA_ID NUMBER(38,0),
	DATABASE_NAME VARCHAR(16777216),
	TASK_DATABASE_ID NUMBER(38,0),
	SCHEDULED_TIME TIMESTAMP_LTZ(9),
	COMPLETED_TIME TIMESTAMP_LTZ(9),
	STATE VARCHAR(16777216),
	RETURN_VALUE VARCHAR(16777216),
	QUERY_ID VARCHAR(16777216),
	QUERY_START_TIME TIMESTAMP_LTZ(9),
	ERROR_CODE VARCHAR(16777216),
	ERROR_MESSAGE VARCHAR(16777216),
	GRAPH_VERSION NUMBER(38,0),
	RUN_ID NUMBER(38,0),
	ROOT_TASK_ID VARCHAR(16777216),
	SCHEDULED_FROM VARCHAR(12),
	INSERT_TIME TIMESTAMP_NTZ(9),
	WAREHOUSE_NAME VARCHAR(16777216),
	SCHEDULE VARCHAR(16777216)
);


CREATE  TABLE  IF NOT EXISTS WAREHOUSE_USER_SPEND (
	WAREHOUSE_NAME VARCHAR(16777216),
	ROLE_NAME VARCHAR(16777216),
	USER_NAME VARCHAR(16777216),
	DATE TIMESTAMP_NTZ(9),
	CREDITS_BY_USER NUMBER(38,3),
	INSERT_TIME TIMESTAMP_NTZ(9)
);



-----------------------------PROCEDURES-----------------------------------------------------



CREATE OR REPLACE PROCEDURE "SP_AUDIT_LOG"()
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS $$

                                            try{
                                                snowflake.execute({sqlText: "BEGIN"});
                                                snowflake.execute({sqlText: "create or replace temporary table v2  as ("+
                                                    "select t.*, TO_TIMESTAMP_NTZ(CONVERT_TIMEZONE('UTC',current_timestamp)) as INSERT_TIME ," +
                                                    "   parse_json('NULL') as table_list " +
                                                    "from table(information_schema.query_history(RESULT_LIMIT=>10000)) t " +
                                                   //",(select array_agg(table_name) as all_tables from snowflake.account_usage.tables) " +
                                                    "where START_TIME > coalesce((select max(START_TIME) as LASTDATE from AUDIT_LOG ),dateadd(DAY,-7,current_timestamp))" +
                                                    "order by START_TIME)"});

                                                snowflake.execute({sqlText: "MERGE INTO AUDIT_LOG using V2 "+
                                                    " on AUDIT_LOG.query_id = v2.query_id and  AUDIT_LOG.execution_status='RUNNING' "+
                                                    " when matched  then "+
                                                    "    update set execution_status='RUNNING_UPDATED'"});
                                                    
                                                snowflake.execute({sqlText: "insert into audit_log " + 
                                                " select v2.*  from v2"});
                                                snowflake.execute({sqlText:"COMMIT"})
                                            } catch(err){
                                                snowflake.execute({sqlText: "ROLLBACK"});
                                                return "Failed";
                                            }
                                        return "Successful";

                                        $$;

CREATE OR REPLACE FUNCTION "ARRAY_MATCHING"("A" ARRAY, "B" ARRAY)
RETURNS ARRAY
LANGUAGE JAVASCRIPT
AS '
    return A.filter(x => B.indexOf(x) !== -1).filter((x, i, a) => a.indexOf(x) == i)
';



CREATE OR REPLACE PROCEDURE "SP_TABLE_STORAGE"()
RETURNS VARCHAR(100)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS $$
                                        var temp_olddistinct = 
                                        "create or replace temp table olddistinct as select ID,MAX(USAGE_DATE) USAGE_DATE from TABLE_STORAGE_ARCHIVE  group by ID";
                                        
                                        var temp_new = 
                                        "create or replace temp table newdata as"+
                                        "	select n.id, TO_TIMESTAMP_NTZ(CONVERT_TIMEZONE('GMT',current_timestamp)) as USAGE_DATE, " +
                                        "           n.table_created,n.table_catalog,n.table_schema,n.table_name, "+
                                        "           n.active_bytes,n.TIME_TRAVEL_BYTES,n.FAILSAFE_BYTES,n.RETAINED_FOR_CLONE_BYTES, "+
                                        "           (n.active_bytes+n.TIME_TRAVEL_BYTES+n.FAILSAFE_BYTES+n.RETAINED_FOR_CLONE_BYTES) AS STORAGE_BYTES, "+
                                        "           TO_TIMESTAMP_NTZ(CONVERT_TIMEZONE('UTC',current_timestamp)) as INSERT_TIME " +
                                        "               from INFORMATION_SCHEMA.table_storage_metrics n " +
                                        "               where n.table_dropped is Null and n.id not in(select id from olddistinct) ";
                                        var temp_max = 
                                        "create or replace temp table maxdata as  "+
                                        "select t.* from TABLE_STORAGE_ARCHIVE t inner join "+
                                        " 	olddistinct t1 on (t.ID=t1.ID and t.USAGE_DATE=t1.USAGE_DATE)";
                                        var temp_update = 
                                        "create or replace temp table existingdata as"+
                                        "	select n.id,TO_TIMESTAMP_NTZ(CONVERT_TIMEZONE('GMT',current_timestamp)) as USAGE_DATE, "+
                                        "			n.table_created,n.table_catalog,n.table_schema,n.table_name, "+
                                        "           n.active_bytes,n.TIME_TRAVEL_BYTES,n.FAILSAFE_BYTES,n.RETAINED_FOR_CLONE_BYTES, "+
                                        "           (n.active_bytes+n.TIME_TRAVEL_BYTES+n.FAILSAFE_BYTES+n.RETAINED_FOR_CLONE_BYTES - t.STORAGE_BYTES) AS STORAGE_BYTES, "+
                                        "           TO_TIMESTAMP_NTZ(CONVERT_TIMEZONE('UTC',current_timestamp)) as INSERT_TIME " +
                                        "           from INFORMATION_SCHEMA.table_storage_metrics n, maxdata t " +
                                        "			where n.id = t.id  and n.table_dropped is Null 	";
                                        
                                        var sql_query = 
                                        "insert into TABLE_STORAGE_ARCHIVE(ID,USAGE_DATE, TABLE_CREATED, TABLE_CATALOG, TABLE_SCHEMA,TABLE_NAME," +
                                        " 		ACTIVE_BYTES,TIME_TRAVEL_BYTES,FAILSAFE_BYTES,RETAINED_FOR_CLONE_BYTES, STORAGE_BYTES,INSERT_TIME) "+
                                        " select  * from newdata "+
                                        "union "+
                                        " select * from existingdata" ;
                                        try{
                                            snowflake.execute({sqlText: "BEGIN"});
                                            snowflake.execute({sqlText: temp_olddistinct});
                                            snowflake.execute({sqlText: temp_new});
                                            snowflake.execute({sqlText: temp_max});
                                            snowflake.execute({sqlText: temp_update});
                                            snowflake.execute({sqlText: sql_query});
                                            snowflake.execute({sqlText: "COMMIT"});
                                        } catch(err){
                                            snowflake.execute({sqlText: "ROLLBACK"});
                                            return "Failed";
                                        }
                                        return "Succeeded.";
                                $$;


CREATE OR REPLACE PROCEDURE "SP_PIPE_DAILY_ACTIVITY_LOG"()
RETURNS VARCHAR(100)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS $$
                                        var temp_history = 
                                         "create or replace temp table history as"+
                                        " select * from snowflake.account_usage.pipe_usage_history where start_time:: date = CURRENT_DATE-1";
                                        
                                        var temp_masterlist = 
                                        "create or replace temp table masterlist as"+
                                        " select pipe_id from history " +
                                        " union "+
                                        " select pipe_id from snowflake.account_usage.pipes where deleted is null ";
                                        
                                         var sql_del_overlapp = 
                                        "delete from PIPE_DAILY_ACTIVITY_LOG"+
                                        " where ref_date = CURRENT_DATE-1";
                                        
                                        var sql_query = 
                                        "INSERT INTO PIPE_DAILY_ACTIVITY_LOG (" +
                                        "select "+
                                        "CURRENT_DATE-1 as ref_date, "+
                                        "ml.pipe_id, "+
                                        "p.pipe_name,"+
                                        " p.pipe_schema,"+
                                        " p.pipe_catalog,"+
                                        " p.pipe_owner,"+
                                        " p.created,"+
                                        " coalesce (psh.end_time::date,p.created::date) as last_data_process_date,"+
                                        " datediff(day, last_data_process_date, ref_date) days_since_data_process,"+
                                        " SPLIT_PART(SPLIT_PART(P.DEFINITION,' ',3) ,'(',1) as TARGET_TABLE ,"+
                                        " TO_TIMESTAMP_NTZ(CONVERT_TIMEZONE('UTC',current_timestamp)) as INSERT_TIME,"+
                                        " ROUND(sum(coalesce (h.credits_used,0)),5) as daily_credits_used,"+
                                        " sum(coalesce (h.bytes_inserted,0)) as daily_bytes_inserted,"+
                                        " sum(coalesce (h.files_inserted,0)) as daily_files_inserted,"+
                                        " count(h.pipe_id) as daily_pipes_exe_count,"+
                                        " sum(coalesce (datediff(second, H.START_TIME, H.END_TIME),0)) AS  DAILY_PIPES_EXE_TIME "+
                                        " from masterlist ml "+
                                        " left join history h on ml.pipe_id = h.pipe_id "+
                                        " left join snowflake.account_usage.pipes p on ml.pipe_id = p.pipe_id "+
                                        " left join (select pipe_id, max(end_time) as end_time from snowflake.account_usage.pipe_usage_history WHERE start_time:: date <=CURRENT_DATE-1 group by 1) psh  "+
                                        " on ml.pipe_id = psh.pipe_id  "+
                                        " group by 1,2,3,4,5,6,7,8,9,10,11)" ;

                                        try{
                                            snowflake.execute({sqlText: "BEGIN"});
                                            snowflake.execute({sqlText: temp_history});
                                            snowflake.execute({sqlText: temp_masterlist});
                                            snowflake.execute({sqlText: sql_del_overlapp});
                                            snowflake.execute({sqlText: sql_query});
                                            snowflake.execute({sqlText: "COMMIT"});
                                        } catch(err){
                                            snowflake.execute({sqlText: "ROLLBACK"});
                                            return "Failed";
                                        }
                                        return "Succeeded.";
                                $$;
								
								
								
CREATE OR REPLACE PROCEDURE "SP_WAREHOUSE_USER_SPEND"()
RETURNS VARCHAR(100)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS $$

                                                
                                            var stmt = snowflake.createStatement( {sqlText: `select count(*) from WareHouse_User_Spend`} );
                                            var resultSet = stmt.execute();
                                            resultSet.next();
                                            
                                            if(resultSet.getColumnValue(1) === 0){
                                                
                                                var temp_wh = "create or replace temp table Warehouse_Spend as (select sum(total_elapsed_time) Total_Elapsed, warehouse_name, date_trunc('DAY',CONVERT_TIMEZONE('UTC', START_TIME)) date "+
                                                            "from SNOWFLAKE.ACCOUNT_USAGE.QUERY_HISTORY group by warehouse_name, date) ";
                                                var temp_user = "create or replace temp table User_Spend as (select sum(total_elapsed_time) Total_Elapsed, warehouse_name, role_name, user_name, date_trunc('DAY',CONVERT_TIMEZONE('UTC', START_TIME)) date "+ 
                                                            "from SNOWFLAKE.ACCOUNT_USAGE.QUERY_HISTORY group by warehouse_name, role_name, user_name, date) ";
                                                var temp_credit = "create or replace temp table Credits_Used as (select sum(Credits_used) Credits_Used, warehouse_name, date_trunc('DAY',CONVERT_TIMEZONE('UTC', START_TIME)) date " +
                                                            "from SNOWFLAKE.ACCOUNT_USAGE.WAREHOUSE_METERING_HISTORY group by warehouse_name, date) ";
                                                var insert_qry = "insert into WareHouse_User_Spend(WAREHOUSE_NAME, ROLE_NAME,USER_NAME,DATE,CREDITS_BY_USER,INSERT_TIME) "+
                                                            "select User_Spend.Warehouse_Name, User_Spend.Role_Name, User_Spend.User_Name, cast(CONVERT_TIMEZONE('UTC', User_spend.date) as TIMESTAMP_NTZ), "+
                                                            "(User_Spend.Total_Elapsed / Warehouse_Spend.Total_Elapsed ) * Credits_Used.Credits_Used as Credits_By_User , TO_TIMESTAMP_NTZ(CONVERT_TIMEZONE('UTC',current_timestamp)) as INSERT_TIME  " +
                                                            "FROM Credits_Used "+
                                                            "join User_Spend on (User_Spend.Warehouse_Name = Credits_Used.Warehouse_Name and User_Spend.date = Credits_Used.date) "+
                                                            "join Warehouse_Spend on (Warehouse_Spend.warehouse_Name = Credits_Used.Warehouse_Name and Warehouse_Spend.date = Credits_Used.date ) "+
                                                            "order by 1,2, 3, 4 ";
                                            }else{
                                                var temp_wh = "create or replace temp table Warehouse_Spend as (select sum(total_elapsed_time) Total_Elapsed, warehouse_name, date_trunc('DAY',CONVERT_TIMEZONE('UTC', START_TIME)) date "+
                                                            "from SNOWFLAKE.ACCOUNT_USAGE.QUERY_HISTORY where date_trunc('DAY',CONVERT_TIMEZONE('UTC', START_TIME))= date_trunc('DAY',DATEADD('day', -1 ,CONVERT_TIMEZONE('UTC', current_date)))  group by warehouse_name, date) ";
                                                var temp_user = "create or replace temp table User_Spend as (select sum(total_elapsed_time) Total_Elapsed, warehouse_name, role_name, user_name, date_trunc('DAY',CONVERT_TIMEZONE('UTC', START_TIME)) date "+ 
                                                            "from SNOWFLAKE.ACCOUNT_USAGE.QUERY_HISTORY where date_trunc('DAY',CONVERT_TIMEZONE('UTC', START_TIME))= date_trunc('DAY',DATEADD('day', -1 ,CONVERT_TIMEZONE('UTC', current_date))) group by warehouse_name, role_name, user_name, date) ";
                                                var temp_credit = "create or replace temp table Credits_Used as (select sum(Credits_used) Credits_Used, warehouse_name, date_trunc('DAY',CONVERT_TIMEZONE('UTC', START_TIME)) date " +
                                                            "from SNOWFLAKE.ACCOUNT_USAGE.WAREHOUSE_METERING_HISTORY where date_trunc('DAY',CONVERT_TIMEZONE('UTC', START_TIME))= date_trunc('DAY',DATEADD('day', -1 ,CONVERT_TIMEZONE('UTC', current_date)))  group by warehouse_name, date) ";
                                                var insert_qry = "insert into WareHouse_User_Spend(WAREHOUSE_NAME, ROLE_NAME,USER_NAME,DATE,CREDITS_BY_USER,INSERT_TIME) "+
                                                            "select User_Spend.Warehouse_Name, User_Spend.Role_Name, User_Spend.User_Name, cast(CONVERT_TIMEZONE('UTC', User_spend.date) as TIMESTAMP_NTZ), "+
                                                            "(User_Spend.Total_Elapsed / Warehouse_Spend.Total_Elapsed ) * Credits_Used.Credits_Used as Credits_By_User, TO_TIMESTAMP_NTZ(CONVERT_TIMEZONE('UTC',current_timestamp)) as INSERT_TIME " +
                                                            "FROM Credits_Used "+
                                                            "join User_Spend on (User_Spend.Warehouse_Name = Credits_Used.Warehouse_Name and User_Spend.date = Credits_Used.date ) "+
                                                            "join Warehouse_Spend on (Warehouse_Spend.warehouse_Name = Credits_Used.Warehouse_Name and Warehouse_Spend.date = Credits_Used.date ) "+
                                                            "order by 1,2, 3, 4";

                                            }
                                            snowflake.execute({sqlText: "BEGIN"});
                                            snowflake.execute({sqlText: temp_wh});
                                            snowflake.execute({sqlText: temp_user});
                                            snowflake.execute({sqlText: temp_credit});
                                            snowflake.execute({sqlText: insert_qry});
                                            snowflake.execute({sqlText: "COMMIT"});
                                            return "Succeeded.";
                                        $$;
										
										
	
	
	 CREATE OR REPLACE PROCEDURE "SP_TASK_LOG"()
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS $$                    var temp_new = "show tasks in account";

                                            try{
                                                snowflake.execute({sqlText: "BEGIN"});
                                                snowflake.execute({sqlText: temp_new});
                                                snowflake.execute({sqlText: `  
                                                create or replace temp table TASK_DETAILS as
                                                select "id"AS ID,"warehouse" AS WAREHOUSE_NAME ,"schedule" AS SCHEDULE from table(result_scan(last_query_id()))a `  
                                                });    
                                                snowflake.execute({sqlText: "insert into TASK_HISTORY_LOG " + 
                                                " select t.*, TO_TIMESTAMP_NTZ(CONVERT_TIMEZONE('UTC',current_timestamp)) as INSERT_TIME,WAREHOUSE_NAME,SCHEDULE from SNOWFLAKE.ACCOUNT_USAGE.TASK_HISTORY t "  + 
                                                "left join TASK_DETAILS u on t.ROOT_TASK_ID = U.ID " + 
                                                "where COMPLETED_TIME > COALESCE((select max(COMPLETED_TIME) as LASTDATE from TASK_HISTORY_LOG),dateadd(MONTH, -2,current_timestamp) ) " 
                                                });
                                                snowflake.execute({sqlText:"COMMIT"})
                                            } catch(err){
                                                snowflake.execute({sqlText: "ROLLBACK"});
                                                return "Failed";
                                            }
                                        return "Successful";

                                        $$;
										
										
										
										
CREATE OR REPLACE PROCEDURE "SP_LOGIN_HISTORY"()
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS $$

	var source_table = "";
	var copy_into_cmd = "";
	var fname = "";
	var ins_stmt = "";
	
	snowflake.execute({sqlText:"create or replace temporary table tmp_log_history as "+
					" select t.*, TO_TIMESTAMP_NTZ(CONVERT_TIMEZONE('UTC',current_timestamp)) as INSERT_TIME  from (SNOWFLAKE.ACCOUNT_USAGE.login_history) t " +
					" where event_timestamp >coalesce((select max(event_timestamp) as last_event_timestamp from " + "LOGIN_HISTORY_LOG),dateadd(DAY,-7,current_timestamp))" +
					"order by event_timestamp "}); 
    


	snowflake.execute({sqlText: `CREATE OR REPLACE TEMPORARY TABLE TMP_GEO_ENRICH(ipv4 number,COUNTRY_NAME VARCHAR(100), CITY_NAME VARCHAR(100),LATITUDE VARCHAR(100),LONGITUDE VARCHAR(100),POSTAL_CODE VARCHAR(100))`});

	snowflake.execute({sqlText: "INSERT INTO TMP_GEO_ENRICH(ipv4,COUNTRY_NAME,CITY_NAME,LATITUDE,LONGITUDE,POSTAL_CODE) " +
			"SELECT  NVL(try_to_number(parse_ip(rawdta.client_ip, 'INET',1):ipv4::VARCHAR),0) as ipv4,Null as COUNTRY_NAME,Null as CITY_NAME,Null as LATITUDE,Null as LONGITUDE,Null as POSTAL_CODE " +
			"FROM "+ 
			"(select distinct client_ip from tmp_log_history) rawdta"});
			
			
	snowflake.execute({sqlText: "update TMP_GEO_ENRICH rawdta "+
						" set rawdta.COUNTRY_NAME=GCB.COUNTRY, "+
						"	rawdta.CITY_NAME=GCB.CITY, "+
					    "	rawdta.LATITUDE=GCB.LAT, "+
						"	rawdta.LONGITUDE=GCB.LNG, "+
						"	rawdta.POSTAL_CODE=GCB.POSTAL "+
						" from ipinfo.public.Location GCB "+
						" where ipinfo.public.TO_JOIN_KEY(BITAND(BITSHIFTRIGHT(rawdta.ipv4,24),255)::text || '.' "+
						"	|| BITAND(BITSHIFTRIGHT(rawdta.ipv4,16),255)::text || '.' "+
						"	|| BITAND(BITSHIFTRIGHT(rawdta.ipv4,8),255)::text || '.' "+
						"	|| BITAND(rawdta.ipv4,255)::text) = GCB.join_key and "+
						" rawdta.ipv4 BETWEEN  GCB.start_ip_int AND GCB.end_ip_int "});
		
        
	snowflake.execute({sqlText: "insert into LOGIN_HISTORY_LOG (  " +
			"EVENT_TIMESTAMP, "  +
			"EVENT_ID, "  +
			"EVENT_TYPE, "  +
			"USER_NAME, "  +
			"CLIENT_IP, "  +
			"REPORTED_CLIENT_TYPE, "  +
			"REPORTED_CLIENT_VERSION, "  +
			"FIRST_AUTHENTICATION_FACTOR, "  +
			"SECOND_AUTHENTICATION_FACTOR, "  +
			"IS_SUCCESS," +
			"ERROR_CODE, "  +
			"ERROR_MESSAGE, "  +
			"RELATED_EVENT_ID, "  +
			"INSERT_TIME, "  +
			"ENRICHMENT_JSON ) " + 
			"select    " +   
			"EVENT_TIMESTAMP, "  +
			"EVENT_ID, "  +
			"EVENT_TYPE, "  +
			"USER_NAME, "  +
			"CLIENT_IP, "  +
			"REPORTED_CLIENT_TYPE, "  +
			"REPORTED_CLIENT_VERSION, "  +
			"FIRST_AUTHENTICATION_FACTOR, "  +
			"SECOND_AUTHENTICATION_FACTOR, "  +
			"IS_SUCCESS," +
			"ERROR_CODE, "  +
			"ERROR_MESSAGE, "  +
			"RELATED_EVENT_ID, "  +
			"INSERT_TIME, "  +
			"OBJECT_CONSTRUCT( " +
							    	"'CLIENT_GEO_COUNTRY',SRC_GCB.COUNTRY_NAME, " +
							    	"'CLIENT_GEO_CITY', SRC_GCB.CITY_NAME, " +
							    	"'CLIENT_GEO_LAT', SRC_GCB.LATITUDE, " +
							    	"'CLIENT_GEO_LONG', SRC_GCB.LONGITUDE " +
								    ") as ENRICHMENT_JSON "  +	
            " from tmp_log_history rawdta LEFT JOIN TMP_GEO_ENRICH  SRC_GCB ON "  +
                           " NVL(try_to_number(parse_ip(rawdta.client_ip, 'INET',1):ipv4::VARCHAR),0) = SRC_GCB.ipv4 "  							
					 }); 
		
return "Succeeded";
$$;									
										
		


		CREATE OR REPLACE PROCEDURE "SP_LOAD_FAILURE"()
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS $$
/*
// Snowpipe Errors Capture

	var loaderror = " select * from snowflake.account_usage.copy_history  where Status = 'Load failed'  and    last_load_time > dateadd(days, -1, current_timestamp()) order by last_load_time "; 
	
	var result= snowflake.createStatement({ sqlText: loaderror}).execute();
	var insresult;
    var pipe_failure="True";
	while (result.next())  {
		// whether pipe failure
        PIPE_CHECK = result.getColumnValue('PIPE_NAME');
        
		if(PIPE_CHECK == null || PIPE_CHECK == undefined ){
			pipe_failure="False";
		}else{
			pipe_failure="True";
		}
		if(pipe_failure === "True"){
			var FileName= result.getColumnValue('FILE_NAME');
			var Stage= result.getColumnValue('STAGE_LOCATION');
			var Errdate = result.getColumnValue('PIPE_RECEIVED_TIME');
			pipe_catalog_name = result.getColumnValue('PIPE_CATALOG_NAME');
			pipe_schema_name = result.getColumnValue('PIPE_SCHEMA_NAME');
            var msg = result.getColumnValue('FIRST_ERROR_MESSAGE').replace(/'/g, "''").replace(/\\\\/g, '');
			var Message = "Error Message:" + msg + "\\n  Table Name:" + pipe_catalog_name + "." + pipe_schema_name + "." + result.getColumnValue('TABLE_NAME') ;
			var SourceType = "Pipe";
			var Code = "000";
			var State = "000";
			
			var errins = "insert into Error(ERRDATE,CODE,STATE,MESSAGE,STACKTRACETXT,SOURCETYPE, FILENAME, STAGE) "+ 
							" values(to_varchar(  to_timestamp_ntz('" + Errdate + "', 'DY MON DD YYYY HH24:MI:SS GMTTZHTZM (TZD)') , 'YYYY-MM-DD HH24:MI:SS'),'" + Code + "','" + State + "','" + Message + "', 'NULL', '" + SourceType + "','" + FileName + "','" + Stage+ "')";
			
			insresult= snowflake.createStatement({ sqlText: errins}).execute();
		}else{
            var tmp = result.getColumnValue('FILE_NAME');
            FileName = tmp.match(/^.*\/(.*)$/g);
            Stage = tmp.match(/^(.*)\/.*$/g);
            //var Stage= result.getColumnValue('STAGE_LOCATION');
			var Errdate = result.getColumnValue('LAST_LOAD_TIME');
			pipe_catalog_name = result.getColumnValue('PIPE_CATALOG_NAME');
			pipe_schema_name = result.getColumnValue('PIPE_SCHEMA_NAME');
            var msg = result.getColumnValue('FIRST_ERROR_MESSAGE').replace(/'/g, "''").replace(/\\/g, '');
			var Message = "Error Message:" + msg + "\n  Table Name:" + pipe_catalog_name + "." + pipe_schema_name + "." + result.getColumnValue('TABLE_NAME') ;
			var SourceType = "CopyInto";
			var Code = "000";
			var State = "000";
			
			var errins = "insert into Error(ERRDATE,CODE,STATE,MESSAGE,STACKTRACETXT,SOURCETYPE, FILENAME, STAGE) "+ 
	                     " values(to_varchar(  to_timestamp_ntz('" + Errdate + "', 'DY MON DD YYYY HH24:MI:SS GMTTZHTZM (TZD)') , 'YYYY-MM-DD HH24:MI:SS'),'" + Code + "','" + State + "','" + Message + "', 'NULL', '" + SourceType + "','" + FileName + "','" + Stage+ "')";
			
			insresult= snowflake.createStatement({ sqlText: errins}).execute();
        }
	}
	*/
	
	
// Stage Load Errors Capture
	
	var loaderror = " select * from information_schema.Load_history  where last_load_time > dateadd(days, -1, current_timestamp()) and Status in ('LOAD_FAILED' ,'PARTIALLY_LOADED') order by  last_load_time ";
	
	var result= snowflake.createStatement({ sqlText: loaderror}).execute();
	var insresult;
	while (result.next())  {
		var status = result.getColumnValue('STATUS'); 
		var FileName = result.getColumnValue('FILE_NAME').match(/^.*\/(.*)$/g);
		var Stage = result.getColumnValue('FILE_NAME').match(/^(.*)\/.*$/g);
		var Errdate = result.getColumnValue('LAST_LOAD_TIME');
		catalog_name = result.getColumnValue('CATALOG_NAME');
		schema_name = result.getColumnValue('SCHEMA_NAME');
		var msg = result.getColumnValue('FIRST_ERROR_MESSAGE').replace(/'/g, "''").replace(/\\/g, '');
		var Message = "Load Status:" + status + ", Error Message:" + msg + "\n  Table Name:" + catalog_name + "." + schema_name + "." + result.getColumnValue('TABLE_NAME') ;
		var SourceType = "Load";
		var Code = "000";
		var State = "000";
		
		var errins = "insert into Error(ERRDATE,CODE,STATE,MESSAGE,STACKTRACETXT,SOURCETYPE, FILENAME, STAGE) "+ 
						" values(to_varchar(  to_timestamp_ntz('" + Errdate + "', 'DY MON DD YYYY HH24:MI:SS GMTTZHTZM (TZD)') , 'YYYY-MM-DD HH24:MI:SS'),'" + Code + "','" + State + "','" + Message + "', 'NULL', '" + SourceType + "','" + FileName + "','" + Stage+ "')";
		
		insresult= snowflake.createStatement({ sqlText: errins}).execute();
	}


// Login Errors Capture
	var loaderror = " select * from snowflake.account_usage.LOGIN_HISTORY where IS_SUCCESS = 'NO' and   EVENT_TIMESTAMP > dateadd(days, -1, current_timestamp())  order by EVENT_TIMESTAMP ";
	
	var result= snowflake.createStatement({ sqlText: loaderror}).execute();
	var insresult;
	while (result.next())  {
		var User = result.getColumnValue('USER_NAME');
		var Client_IP = result.getColumnValue('CLIENT_IP');
		var Errdate = result.getColumnValue('EVENT_TIMESTAMP');
		var Client_Type = result.getColumnValue('REPORTED_CLIENT_TYPE');
		var Client_Version = result.getColumnValue('REPORTED_CLIENT_VERSION');
		var msg = result.getColumnValue('ERROR_MESSAGE').replace(/'/g, "''").replace(/\\/g, '');
		var Message = "Error Code:" + result.getColumnValue('ERROR_CODE') + ", Error Message:" + msg + ", User:" + User + ", Client IP:" + Client_IP + ", Client Type:" + Client_Type + ", Client Version:" + Client_Version   ;
		var SourceType = "Login";
		var Code = result.getColumnValue('ERROR_CODE');
		var State = "000";
		var FileName = "-";
        var Stage = '-';
		
		var errins = "insert into Error(ERRDATE,CODE,STATE,MESSAGE,STACKTRACETXT,SOURCETYPE, FILENAME, STAGE) "+ 
						" values(to_varchar(  to_timestamp_ntz('" + Errdate + "', 'DY MON DD YYYY HH24:MI:SS GMTTZHTZM (TZD)') , 'YYYY-MM-DD HH24:MI:SS'),'" + Code + "','" + State + "','" + Message + "', 'NULL', '" + SourceType + "','" + FileName + "','" + Stage+ "')";
		
		insresult= snowflake.createStatement({ sqlText: errins}).execute();
	}

// Query Failures Errors Capture
//	var loaderror = " select * from snowflake.account_usage.query_history where execution_status = 'FAIL' or execution_status = 'INCIDENT'" +
//    //" and END_TIME > dateadd(days, -30, current_timestamp()) "+ 
//	" and END_TIME > dateadd(days, -1, current_timestamp()) "+ 
//    " order by END_TIME ";
//	
//	var result= snowflake.createStatement({ sqlText: loaderror}).execute();
//	var insresult;
//	var rows = "";
//	while (result.next())  {
//		var User = result.getColumnValue('USER_NAME');
//		var Query_ID = result.getColumnValue('QUERY_ID');
//		var Session_ID = result.getColumnValue('SESSION_ID');
//		var Query_Type = result.getColumnValue('QUERY_TYPE');
//		var Errdate = "to_varchar(  to_timestamp_ntz('" + result.getColumnValue('END_TIME') + "', 'DY MON DD YYYY HH24:MI:SS GMTTZHTZM (TZD)') , 'YYYY-MM-DD HH24:MI:SS')";
//		var Database = result.getColumnValue('DATABASE_NAME');
//		var Schema = result.getColumnValue('SCHEMA_NAME');
//		var msg = result.getColumnValue('ERROR_MESSAGE').replace(/'/g, "''").replace(/\\/g, '');
//		var Message = "Error Code:" + result.getColumnValue('ERROR_CODE') + ", Error Message:" + msg + ", User:" + User + ", Query ID:" + Query_ID + ", Query Type:" + Query_Type + ", Session ID:" + Session_ID  + ", Database:" + Database  + ", Schema:" + Schema   ;
//		var SourceType = "Query";
//		var Code = result.getColumnValue('ERROR_CODE');
//		var State = "000";
//		var FileName = "-";
//		var Stage = "-";
//		
//		rows += "(" + Errdate + ",'" + Code + "','" + State + "','" + Message + "', 'NULL', '" + SourceType + "','" + FileName + "','" + Stage+ "'),";
//	}
//	rows = rows.match(/^(.*),$/g)
//	var errins = "insert into Error(ERRDATE,CODE,STATE,MESSAGE,STACKTRACETXT,SOURCETYPE, FILENAME, STAGE) "+ 
//						" values " + rows ;
//		
//	insresult= snowflake.createStatement({ sqlText: errins}).execute();

return "Successful";
$$;



------------------------------------TASKS---------------------------------------------------------------



create or replace task AUDIT_LOG_TASK
	warehouse={{ warehouse }}
	schedule='USING CRON 0 2 * * * UTC'
	as call SP_AUDIT_LOG();
	
ALTER TASK AUDIT_LOG_TASK RESUME;

create or replace task ERROR_LOAD_TASK
	warehouse={{ warehouse }}
	schedule='USING CRON 0 2 * * * UTC'
	as CALL SP_LOAD_FAILURE();
	
ALTER TASK 	ERROR_LOAD_TASK RESUME;

create or replace task LOGIN_HISTORY_LOG_TASK
	warehouse={{ warehouse }}
	schedule='USING CRON 1 12 * * * UTC'
	as call SP_LOGIN_HISTORY();
ALTER TASK 	LOGIN_HISTORY_LOG_TASK RESUME;

create or replace task PIPE_DAILY_ACTIVITY_TASK
	warehouse={{ warehouse }}
	schedule='USING CRON 0 4 * * * UTC'
	as call SP_PIPE_DAILY_ACTIVITY_LOG();

ALTER TASK PIPE_DAILY_ACTIVITY_TASK RESUME;

create or replace task TABLE_STORAGE_ARCHIVE_TASK
	warehouse={{ warehouse }}
	schedule='USING CRON 0 2 * * * UTC'
	as call SP_TABLE_STORAGE();
	
ALTER TASK TABLE_STORAGE_ARCHIVE_TASK RESUME;


create or replace task TASK_LOG_TASK
	warehouse={{ warehouse }}
	schedule='USING CRON 0 4 * * * UTC'
	as call SP_TASK_LOG();
	
ALTER TASK TASK_LOG_TASK RESUME;

create or replace task WAREHOUSE_USER_SPEND_TASK
	warehouse={{ warehouse }}
	schedule='USING CRON 1 0 * * * UTC'
	as call SP_WareHouse_User_Spend();

ALTER TASK task WAREHOUSE_USER_SPEND_TASK RESUME ;


-------------------------------------VIEWS--------------------------------------------------------------


USE SCHEMA ANALYTICS;


CREATE OR REPLACE VIEW OPERATIONAL_QUERY_LOG_ODM(
	GUID,
	QUERY_TEXT,
	QUERY_TYPE,
	USER_NAME,
	ROLE_NAME,
	WAREHOUSE_NAME,
	WAREHOUSE_SIZE,
	EXECUTION_STATUS,
	EVENT_TIME,
	END_TIME,
	TOTAL_ELAPSED_TIME,
	TOTAL_ELAPSED_TIME_ADJ,
	BYTES_SCANNED,
	OUTBOUND_DATA_TRANSFER_BYTES,
	INBOUND_DATA_TRANSFER_BYTES,
	SUCCESSFUL_QUERY_IND,
	FAILED_QUERY_IND,
	UNIVERSAL_IND,
	COMMIT_TIME,
	OUTBOUND_DATA_TRANSFER_MB,
	TOTAL_ELAPSED_TIME_ADJ_HRS,
	TB_SCANNED,
	GB_SCANNED,
	MB_SCANNED
) as
                                        select 
                                        query_id as GUID,
                                        query_text,
                                        query_type,
                                        USER_NAME,
                                        ROLE_NAME,
                                        warehouse_name,
                                        warehouse_size,
                                        execution_status,
                                        convert_timezone('UTC', start_time) ::timestamp_ntz as EVENT_TIME,
                                        end_time,
                                        total_elapsed_time,
                                        case when end_time :: date != '1969-12-31' then total_elapsed_time end as total_elapsed_time_adj,
                                        bytes_scanned,
                                        outbound_data_transfer_bytes,
                                        inbound_data_transfer_bytes,
                                        case when execution_status = 'SUCCESS' then 1 else 0 end as successful_query_ind,
                                        case when execution_status like 'FAILED%' then 1 else 0 end as failed_query_ind,
                                        1 as universal_ind,
                                        INSERT_TIME as COMMIT_TIME,
                                        outbound_data_transfer_bytes/1000000 as outbound_data_transfer_MB,
                                        total_elapsed_time_adj/3600000 as total_elapsed_time_adj_hrs,
                                         (bytes_scanned)/1E12 as TB_scanned,
                                         (bytes_scanned)/1E9 as GB_scanned,
                                         (bytes_scanned)/1E6 as MB_scanned
                                        from 
                                        "SNOWFLAKEAUDIT".AUDIT_LOG;
										
										
										
										
	CREATE OR REPLACE VIEW QUERY_LOG_ODM(
	GUID,
	QUERY_TEXT,
	TABLE_LIST,
	QUERY_TYPE_INTERNAL,
	QUERY_TYPE,
	PARENT_QUERY_TYPE,
	USER_NAME,
	ROLE_NAME,
	WAREHOUSE_NAME,
	DATABASE_NAME,
	SCHEMA_NAME,
	WAREHOUSE_SIZE,
	EXECUTION_STATUS,
	EVENT_TIME,
	COMPLETED_TIME,
	TOTAL_ELAPSED_TIME,
	TOTAL_ELAPSED_TIME_ADJ,
	BYTES_SCANNED,
	OUTBOUND_DATA_TRANSFER_BYTES,
	INBOUND_DATA_TRANSFER_BYTES,
	SUCCESSFUL_QUERY_IND,
	FAILED_QUERY_IND,
	ACCOUNT_ADMIN_ACT_IND,
	UNIVERSAL_IND,
	COMMIT_TIME,
	OUTBOUND_DATA_TRANSFER_MB,
	TOTAL_ELAPSED_TIME_ADJ_HRS,
	TB_SCANNED,
	GB_SCANNED,
	MB_SCANNED,
	CREDITS_USED_CLOUD_SERVICES,
	ROWS_PRODUCED,
	ERROR_CODE,
	ERROR_MESSAGE,
	SESSION_ID,
	COMPILATION_TIME,
	TRANSACTION_BLOCKED_TIME,
	DVC_PRODUCT,
	DVC_VENDOR,
	DVC_VERSION,
	SRC_TYPE
) as
                                        select 
                                        query_id as GUID,
                                        query_text,
                                        TABLE_LIST,
                                         CASE WHEN  A.query_type = 'UNKNOWN' and ERROR_CODE IS NULL AND replace(TRIM(UPPER(split_part(query_text,' ',1))),'\n', '') <> '' 
                                        then replace(TRIM(UPPER(split_part(query_text,' ',1))),'\n', '')
                                         WHEN --query_type = 'UNKNOWN' and 
                                         ERROR_CODE IS NOT NULL
                                         THEN 'ERROR'   
                                         ELSE A.query_type END AS QUERY_TYPE_INTERNAL,
                                          QUERY_TYPE_INTERNAL AS QUERY_TYPE,
                                         CASE WHEN QUERY_TYPE_INTERNAL LIKE 'ALTER%'THEN 'ALTER'
                                              WHEN QUERY_TYPE_INTERNAL LIKE 'CREATE%'THEN 'CREATE'
                                              WHEN QUERY_TYPE_INTERNAL LIKE 'DROP%'THEN 'DROP'
                                              WHEN QUERY_TYPE_INTERNAL LIKE 'RENAME%'THEN 'RENAME'
                                          ELSE QUERY_TYPE_INTERNAL END AS PARENT_QUERY_TYPE,
                                        USER_NAME,
                                        ROLE_NAME,
                                        warehouse_name,
                                        DATABASE_NAME,
                                        SCHEMA_NAME,
                                        warehouse_size,
                                        execution_status,
                                        convert_timezone('UTC', start_time) ::timestamp_ntz as EVENT_TIME,
                                        convert_timezone('UTC',end_time) ::timestamp_ntz as COMPLETED_TIME,
                                        total_elapsed_time,
                                        case when end_time :: date != '1969-12-31' then total_elapsed_time else 0 end as total_elapsed_time_adj,
                                        bytes_scanned,
                                        outbound_data_transfer_bytes,
                                        inbound_data_transfer_bytes,
                                        case when execution_status = 'SUCCESS' then 1 else 0 end as successful_query_ind,
                                        case when execution_status like 'FAILED%' then 1 else 0 end as failed_query_ind,
                                        case when role_name = 'ACCOUNTADMIN' then 1 else 0 end as account_admin_act_ind,
                                        1 as universal_ind,
                                        INSERT_TIME as COMMIT_TIME,
                                        outbound_data_transfer_bytes/1000000 as outbound_data_transfer_MB,
                                        total_elapsed_time_adj/3600000 as total_elapsed_time_adj_hrs,
                                         (bytes_scanned)/1E12 as TB_scanned,
                                         (bytes_scanned)/1E9 as GB_scanned,
                                         (bytes_scanned)/1E6 as MB_scanned,
                                        CREDITS_USED_CLOUD_SERVICES,
                                           ROWS_PRODUCED,
                                           ERROR_CODE,
                                           ERROR_MESSAGE,
                                           SESSION_ID,
                                           COMPILATION_TIME,
                                           TRANSACTION_BLOCKED_TIME,
                                           'SF_AUDIT' AS DVC_PRODUCT,
                                            'Snowflake' AS DVC_VENDOR,
                                            '*' AS DVC_VERSION,
                                            'Query Logs' AS SRC_TYPE
                                        from 
                                        "SNOWFLAKEAUDIT".AUDIT_LOG A;		


create or replace view ERROR_HISTORY_ODM(
	EVENT_TIME,
	COMMIT_TIME,
	GUID,
	CODE,
	STATE,
	ERROR_MESSAGE,
	STACKTRACETXT,
	SOURCETYPE,
	FILE_NAME,
	STAGE,
	DVC_PRODUCT,
	DVC_VENDOR,
	DVC_VERSION,
	SRC_TYPE
) as 
 select
 ERRDATE as EVENT_TIME,
  CONVERT_TIMEZONE('America/Los_Angeles','UTC',INSERT_TIME) as COMMIT_TIME,
GUID,
CODE ,
STATE,
MESSAGE AS ERROR_MESSAGE,
STACKTRACETXT,
SOURCETYPE,
FILENAME AS FILE_NAME,
STAGE,
'SF_AUDIT' AS DVC_PRODUCT,
'Snowflake' AS DVC_VENDOR,
'*' AS DVC_VERSION,
'Error Logs' AS SRC_TYPE
FROM "SNOWFLAKEAUDIT".ERROR;

					
					
CREATE OR REPLACE VIEW LOGIN_HISTORY_ODM(
	EVENT_TIME,
	GUID,
	EVENT_TYPE,
	USER_NAME,
	CLIENT_IP,
	REPORTED_CLIENT_TYPE,
	REPORTED_CLIENT_VERSION,
	FIRST_AUTHENTICATION_FACTOR,
	SECOND_AUTHENTICATION_FACTOR,
	IS_SUCCESS,
	ERROR_CODE,
	ERROR_MESSAGE,
	RELATED_EVENT_ID,
	COMMIT_TIME,
	ENRICHMENT_JSON
) as
 SELECT 
convert_timezone('UTC', EVENT_TIMESTAMP) ::timestamp_ntz AS EVENT_TIME,
EVENT_ID AS GUID	,
EVENT_TYPE	,
USER_NAME	,
CLIENT_IP	,
REPORTED_CLIENT_TYPE	,
REPORTED_CLIENT_VERSION	,
FIRST_AUTHENTICATION_FACTOR	,
SECOND_AUTHENTICATION_FACTOR	,
IS_SUCCESS	,
ERROR_CODE	,
ERROR_MESSAGE	,
RELATED_EVENT_ID	,
convert_timezone('UTC',EVENT_TIMESTAMP) ::timestamp_ntz AS COMMIT_TIME,
ENRICHMENT_JSON	
FROM "SNOWFLAKEAUDIT".LOGIN_HISTORY_LOG;	



create or replace view PIPE_DAILY_ACTIVITY_ODM(
	PIPE_ID,
	GUID,
	PIPE_NAME,
	PIPE_SCHEMA,
	PIPE_CATALOG,
	PIPE_OWNER,
	CREATED_TIME,
	LAST_DATA_PROCESS_TIME,
	DAYS_SINCE_LAST_DATA_PROCESS,
	TARGET_TABLE,
	DAILY_CREDITS_USED,
	DAILY_BYTES_INSERTED,
	DAILY_FILES_INSERTED,
	DAILY_PIPES_EXE_COUNT,
	DAILY_PIPES_EXE_TIME,
	EVENT_TIME,
	COMMIT_TIME,
	DAILY_PIPES_EXE_MINS,
	DAILY_GB_INSERTED,
	DAILY_MB_INSERTED,
	DVC_PRODUCT,
	DVC_VENDOR,
	DVC_VERSION,
	SRC_TYPE
) as
SELECT  
PIPE_ID,
PIPE_ID||':'||REF_DATE as GUID,
PIPE_NAME,
PIPE_SCHEMA,
PIPE_CATALOG,
PIPE_OWNER,
convert_timezone('UTC', CREATED) ::timestamp_ntz AS CREATED_TIME,
LAST_DATA_PROCESS_DATE::timestamp_ntz AS LAST_DATA_PROCESS_TIME,
DAYS_SINCE_LAST_DATA_PROCESS,
TARGET_TABLE,
DAILY_CREDITS_USED,
DAILY_BYTES_INSERTED,
DAILY_FILES_INSERTED,
DAILY_PIPES_EXE_COUNT,
DAILY_PIPES_EXE_TIME,
REF_DATE::timestamp_ntz AS EVENT_TIME,
INSERT_TIME AS COMMIT_TIME,
ROUND(DAILY_PIPES_EXE_TIME/60,0) as DAILY_PIPES_EXE_MINS,
ROUND((DAILY_BYTES_INSERTED)/1E9,3) as DAILY_GB_INSERTED,
ROUND((DAILY_BYTES_INSERTED)/1E6,1) as DAILY_MB_INSERTED,
'SF_AUDIT' AS DVC_PRODUCT,
'Snowflake' AS DVC_VENDOR,
'*' AS DVC_VERSION,
'PIPE ACTIVITY Logs' AS SRC_TYPE
FROM "SNOWFLAKEAUDIT".PIPE_DAILY_ACTIVITY_LOG
;				



create or replace view TABLE_STORAGE_HISTORY_ODM(
	GUID,
	EVENT_TIME,
	COMMIT_TIME,
	TABLE_CREATED_TIME,
	TABLE_CATALOG,
	TABLE_SCHEMA,
	TABLE_NAME,
	ACTIVE_BYTES,
	TIME_TRAVEL_BYTES,
	FAILSAFE_BYTES,
	RETAINED_FOR_CLONE_BYTES,
	STORAGE_BYTES,
	DVC_PRODUCT,
	DVC_VENDOR,
	DVC_VERSION,
	SRC_TYPE
) as 
  SELECT 
 ID	AS GUID,
USAGE_DATE	AS EVENT_TIME,
INSERT_TIME	AS COMMIT_TIME,
convert_timezone('UTC',TABLE_CREATED) ::timestamp_ntz AS TABLE_CREATED_TIME	,
TABLE_CATALOG	,
TABLE_SCHEMA	,
TABLE_NAME	,
ACTIVE_BYTES	,
TIME_TRAVEL_BYTES	,
FAILSAFE_BYTES	,
RETAINED_FOR_CLONE_BYTES	,
STORAGE_BYTES	,
'SF_AUDIT' AS DVC_PRODUCT,
'Snowflake' AS DVC_VENDOR,
'*' AS DVC_VERSION,
'TABLE STORAGE HISTORY Logs' AS SRC_TYPE
FROM "SNOWFLAKEAUDIT"."TABLE_STORAGE_ARCHIVE";



create or replace view TASK_HISTORY_ODM(
	TASK_NAME,
	QUERY_TEXT,
	CONDITION_TEXT,
	SCHEMA_NAME,
	TASK_SCHEMA_ID,
	DATABASE_NAME,
	TASK_DATABASE_ID,
	EVENT_TIME,
	COMPLETED_TIME,
	COMMIT_TIME,
	STATE,
	RETURN_VALUE,
	GUID,
	ERROR_CODE,
	ERROR_MESSAGE,
	GRAPH_VERSION,
	RUN_ID,
	ROOT_TASK_ID,
	DURATION,
	WAREHOUSE_NAME,
	SCHEDULE,
	QUERY_TYPE,
	PARENT_QUERY_TYPE,
	DVC_PRODUCT,
	DVC_VENDOR,
	DVC_VERSION,
	SRC_TYPE
) as
SELECT  
NAME AS TASK_NAME,
TASK_HISTORY_LOG.QUERY_TEXT,
TASK_HISTORY_LOG.CONDITION_TEXT,
TASK_HISTORY_LOG.SCHEMA_NAME,
TASK_HISTORY_LOG.TASK_SCHEMA_ID,
TASK_HISTORY_LOG.DATABASE_NAME,
TASK_HISTORY_LOG.TASK_DATABASE_ID,
convert_timezone('UTC', COALESCE (TASK_HISTORY_LOG.QUERY_START_TIME,TASK_HISTORY_LOG.SCHEDULED_TIME)) ::timestamp_ntz AS EVENT_TIME,
convert_timezone('UTC',TASK_HISTORY_LOG.COMPLETED_TIME) ::timestamp_ntz AS COMPLETED_TIME,
TASK_HISTORY_LOG.INSERT_TIME AS COMMIT_TIME,
TASK_HISTORY_LOG.STATE,
TASK_HISTORY_LOG.RETURN_VALUE,
COALESCE(TASK_HISTORY_LOG.QUERY_ID,'') AS GUID,
TASK_HISTORY_LOG.ERROR_CODE,
TASK_HISTORY_LOG.ERROR_MESSAGE,
TASK_HISTORY_LOG.GRAPH_VERSION,
TASK_HISTORY_LOG.RUN_ID,
TASK_HISTORY_LOG.ROOT_TASK_ID,
COALESCE (timediff(SECOND, TASK_HISTORY_LOG.QUERY_START_TIME, TASK_HISTORY_LOG.COMPLETED_TIME),0) AS DURATION,
TASK_HISTORY_LOG.WAREHOUSE_NAME,
TASK_HISTORY_LOG.SCHEDULE,
QUERY_TYPE,
PARENT_QUERY_TYPE,
'SF_AUDIT' AS DVC_PRODUCT,
'Snowflake' AS DVC_VENDOR,
'*' AS DVC_VERSION,
'TASK HISTORY Logs' AS SRC_TYPE
FROM SNOWFLAKEAUDIT.TASK_HISTORY_LOG
LEFT JOIN QUERY_LOG_ODM
ON TASK_HISTORY_LOG.QUERY_ID = QUERY_LOG_ODM.GUID
;


create or replace view SECURITY_QUERY_LOG_ODM(
	GUID,
	QUERY_TEXT,
	QUERY_TYPE,
	USER_NAME,
	WAREHOUSE_NAME,
	EXECUTION_STATUS,
	EVENT_TIME,
	END_TIME,
	TOTAL_ELAPSED_TIME,
	TOTAL_ELAPSED_TIME_ADJ,
	SUCCESSFUL_QUERY_IND,
	FAILED_QUERY_IND,
	ACCOUNT_ADMIN_ACT_IND,
	UNIVERSAL_IND,
	COMMIT_TIME,
	WAREHOUSE_SIZE,
	DVC_PRODUCT,
	DVC_VENDOR,
	DVC_VERSION,
	SRC_TYPE
) as
                                        select 
                                        query_id as GUID,
                                        query_text,
                                        query_type,
                                        USER_NAME,
                                        warehouse_name,
                                        execution_status,
                                        convert_timezone('UTC', start_time) ::timestamp_ntz as EVENT_TIME,
                                        end_time,
                                        total_elapsed_time,
                                        case when end_time :: date != '1969-12-31' then total_elapsed_time end as total_elapsed_time_adj,
                                        case when execution_status = 'SUCCESS' then 1 else 0 end as successful_query_ind,
                                        case when execution_status like 'FAILED%' then 1 else 0 end as failed_query_ind,
                                        case when role_name = 'ACCOUNTADMIN' then 1 else 0 end as account_admin_act_ind,
                                        1 as universal_ind,
                                        INSERT_TIME as COMMIT_TIME,
                                        WAREHOUSE_SIZE,
                                        'SF_AUDIT' AS DVC_PRODUCT,
                                        'Snowflake' AS DVC_VENDOR,
                                        '*' AS DVC_VERSION,
                                        'SECURITY QUERY Logs' AS SRC_TYPE
                                        from 
                                    "SNOWFLAKEAUDIT".AUDIT_LOG;
									
									
									
	create or replace view WAREHOUSE_CREDIT_ODM(
	WAREHOUSE_NAME,
	EVENT_TIME,
	GUID,
	CREDITS,
	COMMIT_TIME,
	DVC_PRODUCT,
	DVC_VENDOR,
	DVC_VERSION,
	SRC_TYPE
) as 
 SELECT
 WAREHOUSE_NAME	,
DATE	AS EVENT_TIME,
WAREHOUSE_NAME||':'|| EVENT_TIME::DATE AS GUID,
sum(CREDITS_BY_USER) AS CREDITS	,
MAX(INSERT_TIME)	AS COMMIT_TIME,
'SF_AUDIT' AS DVC_PRODUCT,
'Snowflake' AS DVC_VENDOR,
'*' AS DVC_VERSION,
'WAREHOUSE USER SPEND Logs' AS SRC_TYPE
FROM "SNOWFLAKEAUDIT"."WAREHOUSE_USER_SPEND"
GROUP BY 1,2,3;                                  
                                    
       


	   create or replace view WAREHOUSE_USER_CREDIT_ODM(
	WAREHOUSE_NAME,
	ROLE_NAME,
	USER_NAME,
	EVENT_TIME,
	CREDITS_BY_USER,
	COMMIT_TIME,
	GUID,
	DVC_PRODUCT,
	DVC_VENDOR,
	DVC_VERSION,
	SRC_TYPE
) as 
 SELECT
 WAREHOUSE_NAME	,
ROLE_NAME	,
USER_NAME	,
DATE	AS EVENT_TIME,
CREDITS_BY_USER	,
INSERT_TIME	AS COMMIT_TIME,
'' AS GUID,
'SF_AUDIT' AS DVC_PRODUCT,
'Snowflake' AS DVC_VENDOR,
'*' AS DVC_VERSION,
'WAREHOUSE USER CREDIT Logs' AS SRC_TYPE

FROM "SNOWFLAKEAUDIT"."WAREHOUSE_USER_SPEND";
