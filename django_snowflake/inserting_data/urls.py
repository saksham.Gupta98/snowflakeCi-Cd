
from ast import Add
from django.contrib import admin
from django.urls import path
from inserting_data.polls.add_data import add_rows_api
from inserting_data.polls.snowflake_check_id import check_id
from inserting_data.polls.update_row_api import update_rows
from inserting_data.polls.all_combined_api import all_logic

urlpatterns = [
    path('snowflake/add/rows', add_rows_api.as_view()),
    path('snowflake/check/client_id', check_id.as_view()),
    path('snowflake/update/component_parameters',update_rows.as_view()),
    path('snowflake/check/add/update', all_logic.as_view())

]

