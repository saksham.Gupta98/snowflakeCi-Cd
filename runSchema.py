
import streamlit as st
import requests


def fetch(session, url):
    try:

        url = "https://gitlab.com/api/v4/projects/36705912/trigger/pipeline"

        payload={'ref': 'dev',
        'token': 'd6f33a508e0156902f449780262a7e',
        'variables[RELEASE]': 'dev',
        'variables[INPUTS]': '[{"client_id": "client_tid", "account_name" : "elysiumpartner","username": "app","password":"bigData123$","role": "sysadmin","warehouse": "demo_wh","database": "tenant3","schema": "demo1","component_name": [\"profile\",\"alerts\"],"version": "10"}]'
        }
        files=[

        ]
        headers = {
          'Authorization': 'Bearer Basic ZWFhZG1pbkBzdXBwb3J0ZWx5c2l1bWFuYWx5dGljcy5haTpwYXNzd29yZA=='
        }

        response = requests.request("POST", url, headers=headers, data=payload, files=files)

        print(response.text)
        
        return response.text

    except Exception:
        return {}


def main():
    st.set_page_config(page_title="schema change deployment App", page_icon="🤖")
    st.title("Deploy My schema for Tenant")
    session = requests.Session()
    with st.form("my_form"):
        # index = st.number_input("ID", min_value=0, max_value=100, key="index")
        
        client_id = st.text_input("client_id")
        account_name = st.text_input("account_name")
        username = st.text_input("username")
        password = st.text_input("password")
        role = st.text_input("role")
        warehouse = st.text_input("warehouse")
        database = st.text_input("database")
        schema = st.text_input("schema")
        version_profile = st.number_input("version_profile", min_value=0, max_value=10, key="version_profile")
        version_alerts = st.number_input("version_alerts", min_value=0, max_value=10, key="version_alerts")
        
        
        
        
        submitted = st.form_submit_button("Submit")

        if submitted:
            st.write("Result")
            data = fetch(session, "")
            if data:
                st.subheader('Success')
                st.write(data)
            else:
                st.error("Error")


if __name__ == '__main__':
    main()