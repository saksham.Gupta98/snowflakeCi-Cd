use schema {{ schema }};


-----Sequence

ALTER SEQUENCE ALERT_TBL_SEQ_TEST SET INCREMENT BY 2;


-----Table

ALTER TABLE ALERT_SEARCH rename column USER_ID to USER_NAME;


-----View

CREATE or replace view VW_ALERT_APP_TBL_L3 as 
select 
parent_alert_id as ID,
parent_key ,
ALT.parent_key:alert_type::STRING as alert_NAME,
ALT.ALERT_NAME AS ALERT_TYPE,
ALT.DELETED_FLAG as del_flag,
ALT.PRIORITY as severity,
ALT.ENABLE_NOTIFICATION,
ALT.DESCRIPTION,
ALT.MODIFIED_TS,
EVENT_DATE,
EMAIL1.EMAIL
from 
ALERT_APP_TBL ALT
join VW_ALERT_RULES EMAIL1 on ALT.parent_key:alert_type::STRING=EMAIL1.ID::varchar;


------Function


CREATE OR REPLACE FUNCTION FN_NP_GET_ALERT_GROUPS_NOTIFY(INPUT_MODIFIED_TS VARCHAR(16777216))
RETURNS TABLE (EVENT_DATE TIMESTAMP_NTZ(9), ID VARCHAR(16777216), PARENT_KEY VARIANT, ALERT_RULE VARCHAR(16777216), PARENT_ID VARCHAR(16777216), SRC VARCHAR(16777216), ALERT_TYPE VARCHAR(16777216), DESCRIPTION VARIANT, PRIORITY VARCHAR(16777216), GROUP_ID NUMBER(38,0), DELETED_FLAG BOOLEAN, ENABLE_NOTIFICATION BOOLEAN, EMAIL VARCHAR(16777216), ROLLUP_TIME_VAL NUMBER(38,0), ROLLUP_TIME_UNIT VARCHAR(16777216), MODIFIED_TS TIMESTAMP_NTZ(9), ROW_COUNT NUMBER(38,0))
LANGUAGE SQL
AS '
SELECT 
min(EVENT_DATE) as min_event_date,
id11 as id,
parent_key,                    
parent_key:alert_type ::varchar as alert_type, 
parent_key:parent_id ::varchar as parent_id,
parent_key:src ::varchar as src,
alert_type as alert_type1,
description11,
---description, 
priority as severity,
group_id as main_group_id,
deleted_flag,
enable_notification,
email,
rollup_time_val, 
rollup_time_unit,
max(modified_ts),
count(*) as row_count from
(Select *,first_value(description) over (partition by group_id order by event_date nulls last) as description11,
first_value(id) over (partition by group_id order by event_date nulls last) as id11
FROM table(FN_NP_GET_ALERT_GROUP_NOTIFY_DATA(INPUT_MODIFIED_TS)))
group by 2,3,4,5,6,7,8,9,10,11,12,13,14,15';



--------Procedure


CREATE OR REPLACE PROCEDURE "SP_ALERT_TRIGGER"()
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS '
try
{

  snowflake.execute( {sqlText:''BEGIN''});

  var alert_query_resultset = snowflake.execute( {sqlText: `Select SQL_QUERY from VW_ALERT_RULES_SQL where trigger_frequency = ''Real-time'' or category = ''rule'';`});
  // Loop through the results, processing one row at a time... 
  while (alert_query_resultset.next())  {
    var insert_sql_query = alert_query_resultset.getColumnValue(1);
      snowflake.execute({sqlText: insert_sql_query});
  }

  var source_odm_resultset = snowflake.execute( {sqlText: `Select DISTINCT UPPER(SOURCE_NM) as SOURCE_NM,''COMMIT_TIME'' AS COMMIT_TIME_RPLC from ALERT_RULES;`});
  // Loop through the results, processing one row at a time... 
  while (source_odm_resultset.next())  {

    var update_query = `merge into ALERT_TBL_INGEST_TRACK using (SELECT DISTINCT UPPER(SOURCE_NM) as SOURCE_NM FROM ALERT_RULES WHERE UPPER(SOURCE_NM) = ''` + source_odm_resultset.getColumnValue(1) + `'') AS source_table
    on UPPER(ALERT_TBL_INGEST_TRACK.ODM_NM) = UPPER(source_table.SOURCE_NM)
    when matched then 
        UPDATE  set commit_time = (Select max(` + source_odm_resultset.getColumnValue(2) + `) from ` + source_odm_resultset.getColumnValue(1) + `)
    when not matched then 
        insert (ODM_NM, COMMIT_TIME) values (''` + source_odm_resultset.getColumnValue(1) + `'', (Select max(` + source_odm_resultset.getColumnValue(2) + `) from ` + source_odm_resultset.getColumnValue(1) + `)); `;
      snowflake.execute({sqlText: update_query});
  }

  snowflake.execute( {sqlText:''COMMIT''});
}
catch(err)
{
  snowflake.execute({sqlText: "ROLLBACK"});
  return err;
}
return "The procedure execution completed successfully";
';


------Task

ALTER TASK TASK_NP_ALERT_TRIGGER set warehouse = {{ warehouse }};
