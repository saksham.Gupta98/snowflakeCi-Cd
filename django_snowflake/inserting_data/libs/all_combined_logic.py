from rest_framework import exceptions
from rest_framework.views import APIView
import snowflake.connector
import requests
import json
from ntpath import join
import pandas 
from snowflake.connector.pandas_tools import write_pandas
from snowflake.connector.pandas_tools import pd_writer
import ast

#-------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------

#function to add rows
#getting the data into two lists (column names and row 1)
def add_rows(dic):

    conn = snowflake.connector.connect(
        user = 'NAREN',
        password = 'Naren1997',
        account = 'elysiumpartner',
        role = 'ANALYST',
        warehouse = 'DEMO_WH',
        database = 'TENANT3',
        schema = 'API_TEST'
    )

    cur = conn.cursor()

    t1 = dic
    keys = []
    values = []
        
    for x,y in dic.items():
        keys.append(x)
        values.append(y)

    keys_up = [a.upper() for a in keys]

    row1 = (tuple(values))

    #creating table in snowflake
    create_table = ('''CREATE TABLE test_table_1 IF NOT EXISTS (
    client_id varchar(100),
    account_name varchar(100),
    username varchar(100),
    password varchar(100), 
    role varchar(100),
    warehouse varchar(100),
    database varchar(100),
    schema varchar(100),
    component_name varchar(400), 
    parameters varchar(400),
    commit_time varchar(100)
    )
    ''')

    cur.execute(create_table)

    #retrieving timestamp from snowflake
    time1 = cur.execute('select current_timestamp').fetchone()

    df = pandas.DataFrame([row1], columns= keys_up)
    df['COMMIT_TIME'] = time1
    success, nchunks, nrows, _ = write_pandas(conn, df, "TEST_TABLE_1")
    print(df)
#-------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------

def update_row(dic):
    conn = snowflake.connector.connect(
        user = 'NAREN',
        password = 'Naren1997',
        account = 'elysiumpartner',
        role = 'ANALYST',
        warehouse = 'DEMO_WH',
        database = 'TENANT3',
        schema = 'API_TEST'
    )

    cur = conn.cursor()

    input_component=[]
    input_version = []
    for i in dic["component_name"]:
        input_component.append(i['component'])
        input_version.append(i['version'])

    #getting the component name from the api input in a format that would allow snowflake to recognize it all as a string
    escape_1 = str(dic["component_name"])
    escape_apostrophes = escape_1.replace("'",'"')

    #creating a sql query that would check if basic information(excluding component_id) we entered matches anything in the database
    sql_component = f"""SELECT COMPONENT_NAME FROM test_table_1 WHERE client_id = '{dic['client_id']}'
        AND ROLE = '{dic['role']}'
        AND WAREHOUSE = '{dic['warehouse']}'
        AND DATABASE = '{dic['database']}'
        AND SCHEMA = '{dic['schema']}'
    """

    cur.execute(sql_component)
    snow_row_component = cur.fetchall()

    #if there is a row to be potentially be updated, it will go through this logic
    if snow_row_component:
        #getting the data in the preferred format for component
        x = (snow_row_component[0][0]) 
        sliced = x[1:-1]
        res = ast.literal_eval(sliced)
        #extracting the component_inputs and putting them in a list (to be compared to later with the api component_inputs)
        snowflake_component = []
        snowflake_version = []
        for i in res:
            snowflake_component.append(i['component'])
            snowflake_version.append(i['version'])
            
        #logic if component input needs to be updated
        if snowflake_component != input_component or snowflake_version != input_version:
            sql_update = f"""UPDATE test_table_1 SET component_name = '{escape_apostrophes}' WHERE client_id = '{dic['client_id']} 
            and account_name = '{dic['account_name']}' 
            and role = '{dic['role']}'
            and warehouse = '{dic['warehouse']}'
            and database = '{dic['database']}'
            and schema = '{dic['schema']}'
            """
            cur.execute(sql_update)
            print('component_name has been updated')
            
        elif snowflake_component == input_component and snowflake_version == input_version:
            print('row was not updated since the components are the same')
            
        #sql statement to get the parameters and compare it with input parameters from the api 
        sql_parameters = f"""SELECT PARAMETERS FROM test_table_1 WHERE client_id = '{dic['client_id']}'
        AND ROLE = '{dic['role']}'
        AND WAREHOUSE = '{dic['warehouse']}'
        AND DATABASE = '{dic['database']}'
        AND SCHEMA = '{dic['schema']}'
        """
        cur.execute(sql_parameters)
        snow_row_parameter = cur.fetchall()
        a = (snow_row_parameter[0][0])
        snowflake_parameters = json.loads(a)

        #logic for updating parameters 
        if snowflake_parameters != dic['parameters']:
                input_param = str(dic['parameters'])
                input_param1 = input_param.replace("'",'"')
                sql_update_parameters = f"""UPDATE test_table_1 SET parameters = '{input_param1}' WHERE client_id = '{dic['client_id']}'
                    and account_name = '{dic['account_name']}' 
                    and role = '{dic['role']}'
                    and warehouse = '{dic['warehouse']}'
                    and database = '{dic['database']}'
                    and schema = '{dic['schema']}'
                    """
                cur.execute(sql_update_parameters)
                print('parameters were updated')
               
        elif snowflake_parameters == dic['parameters']:
                print('parameters are the same')
         
    else:
        add_rows(dic)
        print('row has been added')

#-------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------

#main logic combining all the three functions 
def main_logic(client_id_input,dic):
    #creating the snowflake cursor
    conn = snowflake.connector.connect(
    user = 'NAREN',
    password = 'Naren1997',
    account = 'elysiumpartner',
    role = 'ANALYST',
    warehouse = 'DEMO_WH',
    database = 'TENANT3',
    schema = 'API_TEST'
)
    cur = conn.cursor()

    #retrieving previous tenant_ids
    used_ids = cur.execute('select client_id from test_table_1').fetchall()

    ids_used = []
    for i in range(0, len(used_ids)):
        ids_used.append(used_ids[i][0])

    if client_id_input in ids_used:
        update_row(dic)
        return 'row has been updated or added'
        
    else:
        #we're calling the function to add rows
        add_rows(dic)
        return 'new row has been added'
        
#-------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------- 

class ultimate_logic(APIView):
    def logic_function(self,json_input):
        dic = json_input
        client_id_input = json_input['client_id']
        return main_logic(client_id_input, dic)

        