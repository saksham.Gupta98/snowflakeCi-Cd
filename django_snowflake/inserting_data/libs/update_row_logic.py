
from rest_framework import exceptions
from rest_framework.views import APIView
import snowflake.connector
import requests
import json
from ntpath import join
import pandas 
from snowflake.connector.pandas_tools import write_pandas
from snowflake.connector.pandas_tools import pd_writer
import ast

#function to update rows if the given conditions are met(need to update the component name and parameters)
def snowflake_row_update(dic):
    conn = snowflake.connector.connect(
        user = 'NAREN',
        password = 'Naren1997',
        account = 'elysiumpartner',
        role = 'ANALYST',
        warehouse = 'DEMO_WH',
        database = 'TENANT3',
        schema = 'API_TEST'
    )

    cur = conn.cursor()

    #getting the api input component inputs and putting them in two lists 
    input_component=[]
    input_version = []
    for i in dic["component_name"]:
        input_component.append(i['component'])
        input_version.append(i['version'])

    #getting the component name from the api input in a format that would allow snowflake to recognize it all as a string
    escape_1 = str(dic["component_name"])
    escape_apostrophes = escape_1.replace("'",'"')

    #creating a sql query that would check if basic information(excluding component_id) we entered matches anything in the database
    sql_component = f"""SELECT COMPONENT_NAME FROM test_table_1 WHERE client_id = '{dic['client_id']}'
        AND ROLE = '{dic['role']}'
        AND WAREHOUSE = '{dic['warehouse']}'
        AND DATABASE = '{dic['database']}'
        AND SCHEMA = '{dic['schema']}'
    """
    
    cur.execute(sql_component)
    snow_row_component = cur.fetchall()

    #------------------------
    #if there is a row to be potentially be updated, it will go through this logic
    if snow_row_component:
        #getting the data in the preferred format for component
        x = (snow_row_component[0][0]) 
        sliced = x[1:-1]
        res = ast.literal_eval(sliced)
        #extracting the component_inputs and putting them in a list (to be compared to later with the api component_inputs)
        snowflake_component = []
        snowflake_version = []
        for i in res:
            snowflake_component.append(i['component'])
            snowflake_version.append(i['version'])
            
        #logic if component input needs to be updated
        if snowflake_component != input_component or snowflake_version != input_version:
            sql_update = f"UPDATE test_table_1 SET component_name = '{escape_apostrophes}' WHERE client_id = '{dic['client_id']}'"
            cur.execute(sql_update)
            print('component_name has been updated')
            
        elif snowflake_component == input_component and snowflake_version == input_version:
            print('row was not updated since the components are the same')
            
        #-----------------   
        #-----------------
        #sql statement to get the parameters and compare it with input parameters from the api 
        sql_parameters = f"""SELECT PARAMETERS FROM test_table_1 WHERE client_id = '{dic['client_id']}'
        AND ROLE = '{dic['role']}'
        AND WAREHOUSE = '{dic['warehouse']}'
        AND DATABASE = '{dic['database']}'
        AND SCHEMA = '{dic['schema']}'
        """
        cur.execute(sql_parameters)
        snow_row_parameter = cur.fetchall()
        a = (snow_row_parameter[0][0])
        snowflake_parameters = json.loads(a)

        #logic for updating parameters 
        if snowflake_parameters != dic['parameters']:
                input_param = str(dic['parameters'])
                input_param1 = input_param.replace("'",'"')
                sql_update_parameters = f"UPDATE test_table_1 SET parameters = '{input_param1}' WHERE client_id = '{dic['client_id']}'"
                cur.execute(sql_update_parameters)
                print('parameters were updated')
               
        elif snowflake_parameters == dic['parameters']:
                print('parameters are the same')
        
        return 'row has been updated or is already up to date'

    #row cannot be updated 
    else:
        return 'add more information (call the endpoint to add data in table)'


class update_logic(APIView):
    def logic_function(self,json_input):
        dic = json_input
        return snowflake_row_update(dic)

