use schema {{ schema }};

CREATE TABLE SOURCE_INGEST_SUMMARY_LKUP
(
  SOURCE_GROUP STRING, 
     SOURCE_CATEGORY STRING, 
      SOURCE_TYPE STRING, 
      SOURCE STRING,
  INGEST_IND STRING

);

create or replace TABLE INGEST_SUMMARY (
	DATE_TIME TIMESTAMP_NTZ(9),
	INGESTED_COUNT NUMBER(38,0),
	SOURCE VARCHAR(16777216),
	COMMIT_TIME TIMESTAMP_NTZ(9),
	TIME_DIFFERENCE VARCHAR(16777216),
	REFRESH_TIME TIMESTAMP_NTZ(9),
	CURR_IND VARCHAR(16777216)
);

CREATE OR REPLACE PROCEDURE "INGEST_SUMMARY_LOAD"()
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS '
try
{

    snowflake.execute( {sqlText:`BEGIN`});
    
      
     //GET THE SQLS FOR SOURCE AND FREQUENCY
	var source_resultset = snowflake.execute({sqlText: `Select DISTINCT SOURCE FROM ELYSIUM.ENRICHED.SOURCE_INGEST_SUMMARY_LKUP
    WHERE INGEST_IND = $$Y$$
    `});
    
    snowflake.execute({sqlText: `
    create OR REPLACE temp table INGEST_TEMP_TABLE 
    (
    DATE_TIME TIMESTAMP_NTZ(9),
    INGESTED_COUNT numeric,
    SOURCE string,
    COMMIT_TIME  TIMESTAMP_NTZ(9),
     TIME_DIFFERENCE   string
    )
    `});
      
    
	while (source_resultset.next())
    {  
    var table_nm = source_resultset.getColumnValue(1);
      
   snowflake.execute({sqlText: `
  INSERT INTO INGEST_TEMP_TABLE 
   select z.DATE_TIME ,
    z.INGESTED_COUNT, 
    z.SOURCE,
    MAX_COMMIT_TIME AS COMMIT_TIME,
    CASE WHEN z.MAX_COMMIT_TIME = $$1900-01-01 00:00:00.000$$ THEN NULL ELSE 
    DATE_DIFF_WORDS(to_varchar(z.MAX_COMMIT_TIME, $$YYYY-MM-DD HH24:MI:SS$$), 
                    to_varchar(convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9), $$YYYY-MM-DD HH24:MI:SS$$)) 
    END as TIME_DIFFERENCE
    from (
          WITH EVT_TBL AS
          (
          select
            time_slice(time,1,$$HOUR$$,$$START$$) AS start_time,
            sum(case when type = $$event$$ then 1 else 0 end) as ingested_count
            from
          (
          select $$event$$ as type,event_time::TIMESTAMP_NTZ(9) as time
            from `+ table_nm + ` where 
            event_time::TIMESTAMP_NTZ(9) > time_slice(convert_timezone($$EST5EDT$$,DATEADD( hour,-1,CURRENT_TIMESTAMP))::TIMESTAMP_NTZ(9),1, $$HOUR$$)
            and event_time::TIMESTAMP_NTZ(9)<=  time_slice(convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9),1, $$HOUR$$)
          )
          group by start_time
          ) ,
       DATE_GEN AS
      (
      select time_slice(convert_timezone($$EST5EDT$$,DATEADD( hour,-1,CURRENT_TIMESTAMP))::TIMESTAMP_NTZ(9),1, $$HOUR$$) AS date_time
 
      )
            ,  MAX_TM AS
          (
          select
    coalesce (MAX(COMMIT_TIME),$$1900-01-01$$::TIMESTAMP_NTZ(9)) as MAX_COMMIT_TIME
     from `+ table_nm + ` where 
       COMMIT_TIME::TIMESTAMP_NTZ(9) > time_slice(convert_timezone($$EST5EDT$$,DATEADD( hour,-1,CURRENT_TIMESTAMP))::TIMESTAMP_NTZ(9),1, $$HOUR$$)
       and COMMIT_TIME::TIMESTAMP_NTZ(9)<=  time_slice(convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9),1, $$HOUR$$)     
        )    
          select d.date_time, 
      IFNULL(e.ingested_count,0) AS ingested_count, 
     $$`+ table_nm + `$$ AS source
       ,F.MAX_COMMIT_TIME::TIMESTAMP_NTZ(9) as MAX_COMMIT_TIME
       from date_gen d
      left join evt_tbl e
      on d.date_time = e.start_time
        JOIN MAX_TM F 
    )z   
 `});      
      
 }     
      
  var stload_tbl = snowflake.execute( {sqlText:`
            SELECT SOURCE FROM (
            SELECT SOURCE,MAX(COMMIT_TIME) AS COMMIT_TIME  FROM 
            (SELECT SOURCE,COMMIT_TIME FROM INGEST_SUMMARY WHERE COMMIT_TIME  <> $$1900-01-01 00:00:00.000$$
             UNION ALL
             SELECT SOURCE,COMMIT_TIME FROM INGEST_TEMP_TABLE
            )
            INGEST_SUMMARY
            GROUP BY 1
            HAVING COUNT(*) = 1) A
            WHERE COMMIT_TIME  = $$1900-01-01 00:00:00.000$$
             `});     
            
      while (stload_tbl.next())
    { 
    stload_tbl_value = stload_tbl.getColumnValue(1);
            
         snowflake.execute( {sqlText:` update INGEST_TEMP_TABLE t1 set COMMIT_TIME = t2.Commit_time from     
            (select max(Commit_time) as Commit_time from `+ stload_tbl_value + `
          where COMMIT_TIME::TIMESTAMP_NTZ(9)<=  time_slice(convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9),1, $$HOUR$$)
          ) t2
            where t1.SOURCE = $$`+ stload_tbl_value + `$$
            `});  
    }        
            
          snowflake.execute( {sqlText:` update INGEST_TEMP_TABLE t1 set COMMIT_TIME = t2.Commit_time from     
            (
            select SOURCE,max(Commit_time) as Commit_time from INGEST_SUMMARY
            group by 1
          ) t2
            where t1.SOURCE = t2.source
            and t1.COMMIT_TIME =  $$1900-01-01 00:00:00.000$$
            `});  
            
            snowflake.execute( {sqlText:` update INGEST_TEMP_TABLE  set TIME_DIFFERENCE = 
               DATE_DIFF_WORDS(to_varchar(COMMIT_TIME, $$YYYY-MM-DD HH24:MI:SS$$), 
                    to_varchar(convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9), $$YYYY-MM-DD HH24:MI:SS$$))
            where TIME_DIFFERENCE is null
            `});              
            
            snowflake.execute({sqlText: `
delete from INGEST_SUMMARY where DATE_TIME =
time_slice(convert_timezone($$EST5EDT$$,DATEADD( hour,-1,CURRENT_TIMESTAMP))::TIMESTAMP_NTZ(9),1, $$HOUR$$)
    `});
    
            snowflake.execute({sqlText: `
UPDATE INGEST_SUMMARY SET CURR_IND = $$N$$
    `});        
        
    snowflake.execute( {sqlText:`INSERT INTO INGEST_SUMMARY (DATE_TIME,INGESTED_COUNT,SOURCE,COMMIT_TIME,TIME_DIFFERENCE,REFRESH_TIME,CURR_IND)
            SELECT DATE_TIME,INGESTED_COUNT,SOURCE,COMMIT_TIME,TIME_DIFFERENCE,convert_timezone($$EST5EDT$$,CURRENT_TIMESTAMP)::TIMESTAMP_NTZ(9),$$Y$$ 
            FROM INGEST_TEMP_TABLE
            `});      
            

              
            
            	snowflake.execute( {sqlText:`COMMIT`});
}
catch(err)
{
	snowflake.execute({sqlText: "ROLLBACK"});
	return err;
}
return "Succeeded";
';   



              CREATE VIEW VW_INGEST_SUMMARY
              AS 
              
              SELECT DATE_TIME,INGESTED_COUNT,MAIN_VW.SOURCE,
              MAX_COMMIT_TIME,TIME_DIFFERENCE,REFRESH_TIME
              ,SOURCE_GROUP,SOURCE_CATEGORY,SOURCE_TYPE   
              FROM
   ( SELECT   DATE_TIME,INGESTED_COUNT,SOURCE,COMMIT_TIME AS MAX_COMMIT_TIME,TIME_DIFFERENCE,REFRESH_TIME
              FROM INGEST_SUMMARY WHERE CURR_IND = 'Y'
      UNION 
       SELECT A.DATE_TIME,A.INGESTED_COUNT,B.SOURCE,B.COMMIT_TIME AS MAX_COMMIT_TIME,B.TIME_DIFFERENCE,B.REFRESH_TIME
              FROM INGEST_SUMMARY A  
              INNER JOIN (   SELECT   SOURCE,COMMIT_TIME,TIME_DIFFERENCE,REFRESH_TIME
              FROM INGEST_SUMMARY WHERE CURR_IND = 'Y') B
              ON A.SOURCE = B.SOURCE
      WHERE CURR_IND = 'N') MAIN_VW
              INNER JOIN  SOURCE_INGEST_SUMMARY_LKUP LKUP
              ON MAIN_VW.SOURCE = LKUP.SOURCE;
      
	  
	CREATE OR REPLACE TASK  INGESTION_SUMMARY_TASK
  WAREHOUSE ={{ warehouse }}
SCHEDULE=  'USING CRON 1 * * * * EST'
AS
  call INGEST_SUMMARY_LOAD();  
	  
	  
