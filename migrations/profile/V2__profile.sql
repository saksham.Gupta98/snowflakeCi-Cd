use schema {{ schema }};

alter table PROFILE_COND2 ADD CATEGORY String;

create or replace TABLE MITRE_LKUP (
	TACTIC VARCHAR(16777216),
	TECHNIQUE VARCHAR(16777216),
	SUB_TECHNIQUE VARCHAR(16777216),
	TACTIC_ID VARCHAR(16777216),
	TECHNIQUE_ID VARCHAR(16777216),
	SUB_TECHNIQUE_ID VARCHAR(16777216),
	ID VARCHAR(16777216),
	URL VARCHAR(16777216),
	MITRE_SEVERITY VARCHAR(16777216),
	CATEGORY VARCHAR(16777216),
	SORT_ORDER NUMBER(38,0),
	MITRE_SCORE NUMBER(10,2)
);

create or replace view VW_PROFILE_SQL_BUILD_2(
	SOURCE_NAME,
	KEY,
	FREQUENCY,
	ID_LIST,
	TEMP_TABLE,
	SQL,
	MAX_COMMIT_SQL
) as
select
SOURCE_NAME,
KEY,
FREQUENCY,
ID_LIST,
TEMP_TABLE,
'CREATE OR REPLACE TEMP TABLE '||'\n' ||
TEMP_TABLE || ' AS ' ||'\n' ||
'SELECT ' ||'\n' ||
'$$'|| SOURCE_NAME || '$$ AS SOURCE_NAME'|| ','||'\n' ||
ACCT_RPLC || ' AS ACCOUNT_ID'|| ','||'\n' ||
'$$'|| KEY ||'$$ AS KEY'|| ','||'\n' ||
CASE WHEN UPPER(KEY) = 'GLOBAL' THEN '$$ $$' ELSE REPLACE(KEY,',','||$$,$$||') END || ' AS VALUE, ' ||'\n' ||
'TIME_SLICE('|| EVENT_TIME_RPLC ||',1,'|| '$$'|| FREQUENCY || '$$' || ') as BATCH_TIMESTAMP,' ||'\n' ||
'CONVERT_TIMEZONE($$America/Los_Angeles$$,$$UTC$$, current_timestamp::TIMESTAMP_NTZ(9)) AS LOAD_TIMESTAMP,'||'\n' || 
'$$'|| META_ID || '$$ AS PROFILE_NAME'|| ','||'\n' ||
'object_construct('||CNSTR_STRING || ') as DETAIL,' ||'\n' ||
'array_construct(' || ID_LIST || ')::variant as ID_LIST, ' ||'\n' ||
'  MAX(COMMIT_TIME)::timestamp_ntz AS MAX_COMMIT_TIME'||'\n' || 
' FROM   ( select * ' ||'\n' ||
CASE WHEN OBJECT_STRING <> '' THEN ','|| OBJECT_STRING ELSE '' END ||'\n' ||
' FROM   ( select * from ' ||'\n' ||
 SOURCE_NAME ||'\n' || 
 ' WHERE ' || COMMIT_TIME_RPLC ||'\n' ||
 ' > :1' ||'\n' ||
 CASE WHEN UPPER(KEY) = 'GLOBAL' THEN '' ELSE (' AND '||REPLACE(KEY,',',' IS NOT NULL AND ')|| ' IS NOT NULL ') END ||'\n' ||
 ')) MAIN_SQL GROUP BY 1,2,3,4,5,6,7;' AS SQL   ,
 ' SELECT coalesce((Select max(MAX_COMMIT_TIME) from PROFILE_AGG where PROFILE_NAME = $$'||META_ID ||'$$),dateadd($$DAY$$,-10,CONVERT_TIMEZONE($$America/Los_Angeles$$,$$UTC$$, current_timestamp::TIMESTAMP_NTZ(9))))'  as MAX_COMMIT_SQL 
FROM
(select SOURCE_NAME,KEY,FREQUENCY,EVENT_TIME_RPLC,COMMIT_TIME_RPLC,SOURCE_NAME||'|'||KEY||'|'||FREQUENCY AS META_ID,
 SOURCE_NAME||'_'||KEY||'_'||FREQUENCY AS TEMP_TABLE,
 'PRFL_'||SOURCE_NAME||'_'||FREQUENCY||'_STREAM' AS STREAM_NAME,
 SOURCE_NAME||'_'||FREQUENCY||'_STREAM_TBL' AS STREAM_TEMP_TBL,
 ACCT_RPLC,
listagg('$$'||DEFINITION||'$$ ,'||METRIC_CONDITION,',') AS CNSTR_STRING,
listagg((CASE WHEN OBJECT_IND = 'Y' THEN OBJECT_METRIC END ),',') AS OBJECT_STRING,
listagg(ID,',') AS ID_LIST
from PROFILE_COND2
 WHERE ENABLED = 'Y'
 AND  METRIC_TYPE NOT IN ('KVP')
group by 1,2,3,4,5,6,7,8,9,10) A;


CREATE OR REPLACE PROCEDURE "SP_INDEP_PROF_TRIGGER"("SOURCE" VARCHAR(16777216), "FREQ" VARCHAR(16777216))
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS '
try
{

    snowflake.execute( {sqlText:`BEGIN`});
    
    
    //GET THE SQLS FOR SOURCE AND FREQUENCY
	var query_resultset = snowflake.execute({sqlText: `Select TEMP_TABLE,SQL,MAX_COMMIT_SQL
 from VW_PROFILE_SQL_BUILD_2 where SOURCE_NAME = $$` + SOURCE + `$$ AND FREQUENCY = $$` + FREQ + `$$;`});
	while (query_resultset.next())
    {
	var tm_table_nm = query_resultset.getColumnValue(1);
	var insert_sql_query = query_resultset.getColumnValue(2);
    var max_commit_time_sql = query_resultset.getColumnValue(3); 
    
    var max_commit_time_result = snowflake.execute({sqlText: max_commit_time_sql});
     
     while (max_commit_time_result.next())
    { 
    max_commit_time = max_commit_time_result.getColumnValue(1);
     //RUN THE SQLS FOR SOURCE,FREQ,CONTEXT AND POPULATE DATA TO TEMPORARY TABLE. RESULT WIH IN THE FORM OF JSON IN THIS STEP
    snowflake.execute({sqlText: insert_sql_query,
          binds: [max_commit_time]});
    }
    //GET THE SQL FOR DECOUPLING THE JSON INTO SEPARATE ROWS
	var agg_query = snowflake.execute({sqlText: `Select SQL_QRY1,SQL_QRY2,TABLE_NM2 from VW_AGG_SQL_BUILD_2 where TABLE_NM = $$` + tm_table_nm + `$$;`});
	     while (agg_query.next())
		   {
		     var agg_sql_query = agg_query.getColumnValue(1);
    
    //SQL FOR CREATING TEMP TABLE WITH DECOUPLED ROWS. SEPARATE TEMP TABLE FOR ARRAY AND INTEGER      
		     snowflake.execute({sqlText: agg_sql_query});
             var insr_sql_query = agg_query.getColumnValue(2);
    
    //SQL FOR INSERTING DATA FROM TEMP TABLE TO PROFILE AGG.        
		     snowflake.execute({sqlText: insr_sql_query});
     
     //FETCHING TEMP TABLE NAME SO THAT IT CAN BE USED IN NOVELTY DETECTION           
             var tbl_nm2 = agg_query.getColumnValue(3);
            
      //STEP FOR GETTING ALL COMBINATIONS FOR HOURLY NOVELTY DETECTION INCLUDING VALUES
            var hr_novelty_meta =  snowflake.execute({sqlText: `Select distinct A.metric_id,A.batch_timestamp::string from `
              + tbl_nm2 + ` A JOIN PROFILE_COND2 B ON A.metric_id = B.ID
              JOIN PROFILE_AGG_DAYS_LOADED C ON A.metric_id = C.metric_id
             WHERE B.FREQUENCY = $$HOUR$$ AND NOVELTY_IND = $$Y$$
              and A.batch_timestamp > dateadd($$DAY$$,-10,CONVERT_TIMEZONE($$America/Los_Angeles$$,$$UTC$$, current_timestamp::TIMESTAMP_NTZ(9))) 
             `}); 
             
                 while (hr_novelty_meta.next())
		          {  
                    var met_id = hr_novelty_meta.getColumnValue(1);
                    var bt_timestamp = hr_novelty_meta.getColumnValue(2);
                    
      //RUN HOURLY NOVELY          
                    snowflake.execute({sqlText: `INSERT INTO NOVELTY_DETECTION
                                                 select A.*  from table(FN_PROFILE_TRANSFORM_TBL_HOUR(` + met_id + `,$$`+ bt_timestamp + `$$::TIMESTAMP_NTZ(9))) A LEFT JOIN table(FN_PROFILE_TRANSFORM_TBL_BT_HOUR(` + met_id + `,dateadd(hour, -2160, $$`+ bt_timestamp + 
                                                 `$$::TIMESTAMP_NTZ(9)),dateadd(hour, -1, $$`+ bt_timestamp + `$$::TIMESTAMP_NTZ(9)) )) B
                                                  ON A.VALUE = B.VALUE
                                                  AND A.CONTEXT_VALUE = B.CONTEXT_VALUE
                                                  LEFT JOIN NOVELTY_DETECTION C
                                                  ON A.METRIC_ID = C.METRIC_ID
                                                  AND A.BATCH_TIMESTAMP = C.BATCH_TIMESTAMP
                                                  AND A.CONTEXT_VALUE = C.CONTEXT_VALUE
                                                  AND A.VALUE = C.VALUE
                                                 JOIN PROFILE_PREV_DAY_LOADED D
                                                  ON A.METRIC_ID = D.METRIC_ID
                                                  AND A.BATCH_TIMESTAMP = D.BATCH_TIMESTAMP
                                                  AND A.CONTEXT_VALUE = D.CONTEXT_VALUE
                                                  WHERE  B.VALUE IS NULL
                                                  AND  C.VALUE IS NULL
                                                  AND D.PREV_DAY_COUNT > 14;`
                                      }); 
		          }	  
           
           
         //STEP FOR GETTING ALL COMBINATIONS FOR DAILY NOVELTY DETECTION INCLUDING VALUES  
              var dly_novelty_meta =  snowflake.execute({sqlText: `Select distinct A.metric_id,A.batch_timestamp::date::string from `
              + tbl_nm2 + ` A JOIN PROFILE_COND2 B ON A.metric_id = B.ID
              JOIN PROFILE_AGG_DAYS_LOADED C ON A.metric_id = C.metric_id
             WHERE B.FREQUENCY = $$DAY$$ AND NOVELTY_IND = $$Y$$`}); 
             
                 while (dly_novelty_meta.next())
		          {  
                    var met_id = dly_novelty_meta.getColumnValue(1);
                    var bt_timestamp = dly_novelty_meta.getColumnValue(2);
                    snowflake.execute({sqlText: `INSERT INTO NOVELTY_DETECTION
                                                 select A.*  from table(FN_PROFILE_TRANSFORM_TBL_DATE(` + met_id + `,$$`+ bt_timestamp + `$$::date)) A LEFT JOIN table(FN_PROFILE_TRANSFORM_TBL_BETWEENDATES(` + met_id + `,dateadd(day, -90, $$`+ bt_timestamp + 
                                                 `$$::date),dateadd(day, -1, $$`+ bt_timestamp + `$$::date) )) B
                                                  ON A.VALUE = B.VALUE
                                                  AND A.CONTEXT_VALUE = B.CONTEXT_VALUE
                                                  LEFT JOIN NOVELTY_DETECTION C
                                                  ON A.METRIC_ID = C.METRIC_ID
                                                  AND A.BATCH_TIMESTAMP = C.BATCH_TIMESTAMP
                                                  AND A.CONTEXT_VALUE = C.CONTEXT_VALUE
                                                  AND A.VALUE = C.VALUE
                                                  JOIN PROFILE_PREV_DAY_LOADED D
                                                  ON A.METRIC_ID = D.METRIC_ID
                                                  AND A.BATCH_TIMESTAMP = D.BATCH_TIMESTAMP
                                                  AND A.CONTEXT_VALUE = D.CONTEXT_VALUE
                                                  WHERE  B.VALUE IS NULL
                                                  AND  C.VALUE IS NULL
                                                  AND D.PREV_DAY_COUNT > 14 ;`
                                      }); 
		          }	  
                  
             //CUSTOMER ALERTS     
             snowflake.execute({sqlText: `INSERT INTO CUSTOM_ALERTS
             SELECT A.* FROM 
             (Select distinct 
             A.METRIC_DEFINITION,
             A.METRIC_ID,
             A.BATCH_TIMESTAMP,
             A.CONTEXT_KEY,
             A.CONTEXT_VALUE,
             lfo.value::VARCHAR value,
             uuid_string() AS GUID,
             current_timestamp::TIMESTAMP_NTZ(9) AS COMMIT_TIME,
             A.ACCOUNT_ID
             from `
              + tbl_nm2 + ` A , lateral flatten(input=>A.METRIC_VALUE) lfo) A
              JOIN PROFILE_COND2 B ON A.metric_id = B.ID
              WHERE  ALERT_IND = $$Y$$`
                               });      
           }
           
	snowflake.execute({sqlText: `DROP TABLE ` + tm_table_nm + `;`}); 
    
    }
    
	snowflake.execute( {sqlText:`COMMIT`});
}
catch(err)
{
	snowflake.execute({sqlText: "ROLLBACK"});
	return err;
}
return "Succeeded";
';


create or replace view VW_NOVELTY(
	COMMIT_TIME,
	METRIC_ID,
	EVENT_TIME,
	FINDING_UPDATE_TIME,
	ACCOUNT_ID,
	FINDING_ID_D,
	FINDING_TITLE,
	FINDING_TYPE,
	SEVERITY,
	SEVERITY_NUMBER,
	FINDING_COUNT,
	RESOURCE,
	MITRE_ID,
	DATA_SOURCE,
	FINDING_CAT,
	INSTANCE_ID,
	INSTANCE_ID2,
	PRIVATE_IP,
	PUBLIC_IP,
	CATEGORY,
	SRC_USER_NAME,
	SRC_CALLER_IP,
	REMOTE_IP,
	LOCAL_IP,
	S3_BUCKET_NAME,
	FIRST_EVENT_TIME,
	LAST_EVENT_TIME,
	CREATE_TIME,
	INSTANCE_AVAILABILITY_ZONE,
	INSTANCE_LAUNCH_TIME,
	INSTANCE_PROFILE_ARN,
	INSTANCE_PROFILE_ID,
	INSTANCE_STATE,
	INSTANCE_TYPE,
	RESOURCE_TYPE,
	FINDING_ARN,
	FINDING_DESC,
	FINDING_FEEDBACK,
	FINDING_INFO,
	REGION_NAME,
	BUCKET_INFO,
	SRC_USER_TYPE,
	USERAGENT,
	GUID,
	SOURCE,
	SOURCE_SUBTYPE,
	SEV_SCORE,
	INDV_RISK_SCORE
) as
select 
A.COMMIT_TIME,
A.METRIC_ID,
A.EVENT_TIME,
A.LAST_EVENT_TIME  AS FINDING_UPDATE_TIME,
A.ACCOUNT_ID AS ACCOUNT_ID,
GUID AS FINDING_ID_D,
A.FINDING_TITLE,
A.FINDING_TYPE,
A.SEVERITY,
A.SEVERITY_NUMBER,
A.FINDING_COUNT,
NULL AS RESOURCE,
A.MITRE_ID,
NULL AS data_source,
A.FINDING_CAT,
instance_id,
NULL AS instance_id2,
-- private_ip,
A.PRIVATE_IP ,
--  public_ip,
A.PUBLIC_IP ,
A.CATEGORY,
lower(A.SRC_USER_NAME) as SRC_USER_NAME,
NULL AS src_caller_ip,
NULL AS REMOTE_IP,
NULL AS LOCAL_IP,
A.S3_BUCKET_NAME,
A.FIRST_EVENT_TIME,
A.LAST_EVENT_TIME,
NULL AS CREATE_TIME,
NULL AS INSTANCE_AVAILABILITY_ZONE,
NULL AS INSTANCE_LAUNCH_TIME,
NULL AS INSTANCE_PROFILE_ARN,
NULL AS INSTANCE_PROFILE_ID,
NULL AS INSTANCE_STATE,
NULL AS INSTANCE_TYPE,
NULL AS RESOURCE_TYPE,
GUID AS FINDING_ARN,
A.FINDING_DESC,
NULL AS FINDING_FEEDBACK,
NULL AS FINDING_INFO,
NULL AS REGION_NAME,
NULL AS BUCKET_INFO,
NULL AS SRC_USER_TYPE,
NULL AS USERAGENT,
 GUID,
 'EA' AS SOURCE,
 'NOVELTY' AS SOURCE_SUBTYPE,
A.SEV_SCORE, 
A.INDV_RISK_SCORE 
from  
(
select
--uuid_string() AS GUID,
METRIC_DEFINITION,
  A.METRIC_DEFINITION,
METRIC_ID,
CONTEXT_VALUE,
  ACCOUNT_ID,
case when CONTEXT_KEY = 'INSTANCE_ID' THEN CONTEXT_VALUE 
when B.UNIT = 'INSTANCE_ID' THEN VALUE 
end as INSTANCE_ID,
case when CONTEXT_KEY = 'S3_BUCKET_NAME' THEN CONTEXT_VALUE 
when B.UNIT = 'S3_BUCKET_NAME' THEN VALUE 
end as S3_BUCKET_NAME,
case when CONTEXT_KEY = 'SRC_USER_ID' THEN CONTEXT_VALUE 
when B.UNIT = 'SRC_USER_ID' THEN VALUE
end as SRC_USER_NAME,
case when array_size(split(CONTEXT_VALUE,'.')) = 4 and length(CONTEXT_VALUE) between 8 and 16 THEN CONTEXT_VALUE  --------VALUES NEEDED TO BE ADDED
  when B.UNIT in ('RPT_IP','IP_ADDRESS') THEN VALUE
end as PRIVATE_IP,
case  when B.UNIT in ('RPT_IP','IP_ADDRESS') THEN VALUE --------VALUES NEEDED TO BE ADDED
end as PUBLIC_IP,
BATCH_TIMESTAMP as EVENT_TIME,
BATCH_TIMESTAMP as last_event_time,
BATCH_TIMESTAMP as first_event_time,
1 as finding_count,
VALUE as RESULT_VALUES , 
GUID,
A.COMMIT_TIME,
B.CONTEXT_VALUE_DESC||A.CONTEXT_VALUE||B.RESULT_VALUE_DESC||A.VALUE AS FINDING_TITLE,
B.FINDING_TYPE,
B.SEVERITY,
B.SEVERITY_NUMBER,
  B.CATEGORY,
  B.MITRE_ID,
  B.FINDING_CAT,
  B.CONTEXT_VALUE_DESC||A.CONTEXT_VALUE||B.RESULT_VALUE_DESC||A.VALUE AS FINDING_DESC,
    CASE WHEN SEVERITY = 'LOW' THEN (1/3)
                WHEN SEVERITY = 'MEDIUM' THEN (2/3)
                WHEN SEVERITY = 'HIGH' THEN (3/3)
                ELSE NULL END
          AS SEV_SCORE,
(MITRE_SCORE*SEV_SCORE)::DECIMAL(10,2) AS INDV_RISK_SCORE 
from NOVELTY_DETECTION A
JOIN PROFILE_COND2 B
ON A.METRIC_ID = B.ID
LEFT JOIN MITRE_LKUP MITRE_LKUP
ON MITRE_LKUP.ID = B.mitre_id) A
;
