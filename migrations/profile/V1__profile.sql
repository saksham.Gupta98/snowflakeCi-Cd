use schema {{ schema }};
create or replace TABLE PROFILE_COND2 (
	ID NUMBER(10,0),
	SOURCE_NAME VARCHAR(50),
	KEY VARCHAR(50),
	FREQUENCY VARCHAR(50),
	DEFINITION VARCHAR(100),
	METRIC_CONDITION VARCHAR(16777216),
	ENABLED VARCHAR(1),
	OBJECT_METRIC VARCHAR(16777216),
	EVENT_TIME_RPLC VARCHAR(16777216),
	COMMIT_TIME_RPLC VARCHAR(16777216),
	OBJECT_IND VARCHAR(16777216),
	METRIC_TYPE VARCHAR(16777216),
	ACCT_RPLC VARCHAR(16777216),
	REGIION_RPLC VARCHAR(16777216),
	UNIT VARCHAR(16777216),
	MITRE_ID VARCHAR(16777216),
	CONTEXT_VALUE_DESC VARCHAR(16777216),
	RESULT_VALUE_DESC VARCHAR(16777216),
	FINDING_TYPE VARCHAR(16777216),
	FINDING_CAT VARCHAR(16777216),
	SEVERITY VARCHAR(16777216),
	SEVERITY_NUMBER NUMBER(38,0),
	NOVELTY_IND VARCHAR(16777216),
	ANOMOLY_IND VARCHAR(16777216),
	MAX_NOVELTY_TIMESTAMP TIMESTAMP_NTZ(9),
	ALERT_IND VARCHAR(16777216),
	PRFL_RUN_DAYS NUMBER(38,0)
);


create or replace TABLE PROFILE_AGG (
	CONTEXT_KEY VARCHAR(16777216),
	CONTEXT_VALUE VARCHAR(16777216),
	BATCH_TIMESTAMP TIMESTAMP_NTZ(9),
	LOAD_TIMESTAMP TIMESTAMP_NTZ(9),
	FREQUENCY VARCHAR(16777216),
	METRIC_ID NUMBER(38,0),
	METRIC_TYPE VARCHAR(16777216),
	METRIC_DEFINITION VARCHAR(16777216),
	METRIC_VALUE VARIANT,
	PROFILE_NAME VARCHAR(16777216),
	MAX_COMMIT_TIME TIMESTAMP_NTZ(9),
	ACCOUNT_ID VARCHAR(16777216)
);


create or replace TABLE NOVELTY_DETECTION (
	METRIC_DEFINITION VARCHAR(16777216),
	METRIC_ID NUMBER(38,0),
	BATCH_TIMESTAMP TIMESTAMP_NTZ(9),
	CONTEXT_KEY VARCHAR(16777216),
	CONTEXT_VALUE VARCHAR(16777216),
	VALUE VARCHAR(16777216),
	GUID VARCHAR(16777216),
	COMMIT_TIME TIMESTAMP_NTZ(9),
	ACCOUNT_ID VARCHAR(16777216)
);

create or replace TABLE CUSTOM_ALERTS (
	METRIC_DEFINITION VARCHAR(16777216),
	METRIC_ID NUMBER(38,0),
	BATCH_TIMESTAMP TIMESTAMP_NTZ(9),
	CONTEXT_KEY VARCHAR(16777216),
	CONTEXT_VALUE VARCHAR(16777216),
	VALUE VARCHAR(16777216),
	GUID VARCHAR(16777216),
	COMMIT_TIME TIMESTAMP_NTZ(9),
	ACCOUNT_ID VARCHAR(16777216)
);


create or replace view ANALYTICS.VW_PROFILE_SQL_BUILD_2(
	SOURCE_NAME,
	KEY,
	FREQUENCY,
	ID_LIST,
	TEMP_TABLE,
	STREAM_NAME,
	STREAM_TEMP_TBL,
	STREAM_SQL,
	SQL
) as (
    


 
select
SOURCE_NAME,
KEY,
FREQUENCY,
ID_LIST,
TEMP_TABLE,
STREAM_NAME,
STREAM_TEMP_TBL,
'CREATE OR REPLACE TEMP TABLE '||'\n' ||
STREAM_TEMP_TBL || ' AS ' ||'\n' ||
'SELECT * FROM ' ||'\n' ||
STREAM_NAME ||'\n' ||
' WHERE METADATA$ACTION = $$INSERT$$ ' AS STREAM_SQL,
'CREATE OR REPLACE TEMP TABLE '||'\n' ||
TEMP_TABLE || ' AS ' ||'\n' ||
'SELECT ' ||'\n' ||
'$$'|| SOURCE_NAME || '$$ AS SOURCE_NAME'|| ','||'\n' ||
ACCT_RPLC || ' AS ACCOUNT_ID'|| ','||'\n' ||
'$$'|| KEY ||'$$ AS KEY'|| ','||'\n' ||
CASE WHEN UPPER(KEY) = 'GLOBAL' THEN '$$ $$' ELSE REPLACE(KEY,',','||$$,$$||') END || ' AS VALUE, ' ||'\n' ||
'TIME_SLICE('|| EVENT_TIME_RPLC ||',1,'|| '$$'|| FREQUENCY || '$$' || ') as BATCH_TIMESTAMP,' ||'\n' ||
'CURRENT_TIMESTAMP()::timestamp_ntz AS LOAD_TIMESTAMP,'||'\n' || 
'$$'|| META_ID || '$$ AS PROFILE_NAME'|| ','||'\n' ||
'object_construct('||CNSTR_STRING || ') as DETAIL,' ||'\n' ||
'array_construct(' || ID_LIST || ')::variant as ID_LIST, ' ||'\n' ||
'  MAX(COMMIT_TIME)::timestamp_ntz AS MAX_COMMIT_TIME'||'\n' || 
' FROM   ( select * ' ||'\n' ||
CASE WHEN OBJECT_STRING <> '' THEN ','|| OBJECT_STRING ELSE '' END ||'\n' ||
' FROM   ( select * from ' ||'\n' ||
 SOURCE_NAME ||'\n' || 
 ' WHERE ' || COMMIT_TIME_RPLC ||'\n' ||
 ' > coalesce((Select max(MAX_COMMIT_TIME) from PROFILE_AGG where PROFILE_NAME = $$'||META_ID ||'$$),date_trunc($$month$$,to_timestamp_ntz(convert_timezone($$America/Los_Angeles$$, $$UTC$$,current_timestamp ))))' ||'\n' ||
 CASE WHEN UPPER(KEY) = 'GLOBAL' THEN '' ELSE (' AND '||REPLACE(KEY,',',' IS NOT NULL AND ')|| ' IS NOT NULL ') END ||'\n' ||
 ')) MAIN_SQL GROUP BY 1,2,3,4,5,6,7;' AS SQL   
FROM
(select SOURCE_NAME,KEY,FREQUENCY,EVENT_TIME_RPLC,COMMIT_TIME_RPLC,SOURCE_NAME||'|'||KEY||'|'||FREQUENCY AS META_ID,
 SOURCE_NAME||'_'||KEY||'_'||FREQUENCY AS TEMP_TABLE,
 'PRFL_'||SOURCE_NAME||'_'||FREQUENCY||'_STREAM' AS STREAM_NAME,
 SOURCE_NAME||'_'||FREQUENCY||'_STREAM_TBL' AS STREAM_TEMP_TBL,
 ACCT_RPLC,
listagg('$$'||DEFINITION||'$$ ,'||METRIC_CONDITION,',') AS CNSTR_STRING,
listagg((CASE WHEN OBJECT_IND = 'Y' THEN OBJECT_METRIC END ),',') AS OBJECT_STRING,
listagg(ID,',') AS ID_LIST
from  ANALYTICS.PROFILE_COND2
 WHERE ENABLED = 'Y'
 AND  METRIC_TYPE NOT IN ('KVP')
group by 1,2,3,4,5,6,7,8,9,10) A
  );



create or replace view ANALYTICS.VW_AGG_SQL_BUILD_2(
	SOURCE_NAME,
	KEY,
	FREQUENCY,
	METRICS_TYPE,
	TABLE_NM,
	TABLE_NM2,
	SQL_QRY1,
	SQL_QRY2
) as (
    


SELECT SOURCE_NAME,
KEY,
FREQUENCY,
METRICS_TYPE1 AS METRICS_TYPE,
TABLE_NM,
TABLE_NM||'_'||METRICS_TYPE1 as TABLE_NM2,
STR_1||STR_2||STR_3 AS SQL_QRY1,
STR_4 AS SQL_QRY2
FROM
(
  SELECT
  SOURCE_NAME,
KEY,
FREQUENCY,
  CASE WHEN METRIC_TYPE = $$ARRAY$$ THEN 
     $$ARRAY$$
ELSE
     $$INTEGER$$
     END  AS METRICS_TYPE1,
  SOURCE_NAME||'_'||KEY||'_'||FREQUENCY as TABLE_NM,
  
'CREATE OR REPLACE TEMP TABLE '|| SOURCE_NAME||'_'||KEY||'_'||FREQUENCY||'_'||METRICS_TYPE1 ||' AS '||'\n' ||
  'SELECT A.KEY AS CONTEXT_KEY,
A.VALUE AS CONTEXT_VALUE,
A.BATCH_TIMESTAMP,
B.FREQUENCY,
B.ID as METRIC_ID,
B.METRIC_TYPE,
A.METRIC_KEY as METRIC_DEFINITION,
A.LOAD_TIMESTAMP,
A.PROFILE_NAME ,  
A.MAX_COMMIT_TIME,  ' ||'\n' ||
CASE WHEN METRIC_TYPE = $$ARRAY$$ THEN 
     $$ARRAY_FLAT(ARRAY_AGG(METRIC_VALUE::ARRAY))$$
ELSE
     $$ARRAY_CONSTRUCT(SUM(METRIC_VALUE::numeric))$$
     END  || $$ AS METRIC_VALUE $$ ||','||'\n' ||
 'A.ACCOUNT_ID
  FROM
(select A.SOURCE_NAME
 ,A.KEY
 ,A.VALUE
 ,A.BATCH_TIMESTAMP
 ,l1.KEY AS METRIC_KEY
 ,l1.value AS METRIC_VALUE
  ,A.LOAD_TIMESTAMP
 ,A.PROFILE_NAME
  ,A.MAX_COMMIT_TIME
  ,A.ACCOUNT_ID
from ' ||'\n' ||
SOURCE_NAME||'_'||KEY||'_'||FREQUENCY ||' A ' || '\n' ||
',lateral flatten(DETAIL, outer => true)l1
 -- JOIN PROFILE_AGG B
 -- ON A.
WHERE l1.value::STRING <>$$[]$$ AND METRIC_VALUE <> 0 
)A 
  join PROFILE_COND2 B
  ON A.METRIC_KEY  = B.DEFINITION
  AND A.PROFILE_NAME  = B.SOURCE_NAME||$$|$$||B.KEY||$$|$$||B.FREQUENCY
  WHERE B.ID IN (' as STR_1
,LISTAGG(ID, ',') as STR_2
,') GROUP BY 1,2,3,4,5,6,7,8,9,10,12;'
  --|| '\n' ||
  --'DROP TABLE ' || SOURCE_NAME||'_'||KEY||'_'||FREQUENCY ||';'
  as STR_3,
 'INSERT INTO PROFILE_AGG (CONTEXT_KEY,CONTEXT_VALUE,BATCH_TIMESTAMP,FREQUENCY,METRIC_ID,METRIC_TYPE,METRIC_DEFINITION,LOAD_TIMESTAMP,PROFILE_NAME,MAX_COMMIT_TIME,METRIC_VALUE,ACCOUNT_ID)  
  SELECT * FROM ' 
  || SOURCE_NAME||'_'||KEY||'_'||FREQUENCY||'_'||METRICS_TYPE1 || ';'
  AS STR_4

FROM  ANALYTICS.PROFILE_COND2
WHERE   ENABLED = 'Y' AND METRIC_TYPE NOT IN ('KVP')
GROUP BY 1,2,3,4,5,6,8,9
) main_sql
  );
  
  
  
 create or replace view ANALYTICS.PROFILE_AGG_DAYS_LOADED(
	METRIC_ID,
	NO_OF_DAYS
) as

         select  METRIC_ID,
  COUNT(DISTINCT BATCH_TIMESTAMP::DATE) AS NO_OF_DAYS
  FROM PROFILE_AGG A
 WHERE
   BATCH_TIMESTAMP::DATE BETWEEN CURRENT_DATE - 60 AND CURRENT_DATE
   GROUP BY 1
   HAVING NO_OF_DAYS >=10;
   
   create or replace view DEMO_ELYSIUM_DB.ANALYTICS.PROFILE_PREV_DAY_LOADED(
	METRIC_ID,
	BATCH_TIMESTAMP,
	CONTEXT_VALUE,
	PREV_DAY_COUNT
) as (
    


      SELECT  
      A.METRIC_ID,
      A.BATCH_TIMESTAMP,
      A.CONTEXT_VALUE,
   COUNT(DISTINCT B.BATCH_TIMESTAMP) AS PREV_DAY_COUNT FROM 
   ANALYTICS.PROFILE_AGG A JOIN 
   
  ( select  DISTINCT METRIC_ID,
         BATCH_TIMESTAMP,
      CONTEXT_VALUE

  FROM ANALYTICS.PROFILE_AGG A
 WHERE
   BATCH_TIMESTAMP::DATE BETWEEN CURRENT_DATE - 90 AND CURRENT_DATE
  -- GROUP BY 1,2,3
   --HAVING   COUNT(*) >=1
  ) B
   ON A.METRIC_ID =B.METRIC_ID
   AND A.CONTEXT_VALUE = B.CONTEXT_VALUE
   AND B.BATCH_TIMESTAMP <= A.BATCH_TIMESTAMP::DATE -1 
   AND B.BATCH_TIMESTAMP > A.BATCH_TIMESTAMP::DATE -90 
   GROUP BY 1,2,3
  );
  
  
  CREATE OR REPLACE PROCEDURE "SP_PRFL_TASK_BUILDER"("WAREHOUSE" VARCHAR(16777216))
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS '
try
{

      var temp_new = "show tasks like $$PRFL_%$$";

	snowflake.execute( {sqlText:''BEGIN''});
    snowflake.execute({sqlText: temp_new});
    snowflake.execute({sqlText: `create or replace TEMP TABLE TASK_TBL AS 
                                 select "name"AS ACT_TASK_NAME,"warehouse" AS ACT_WAREHOUSE_NAME ,"schedule" AS ACT_SCHEDULE,"state" as TASK_STATE from table(result_scan(last_query_id()))a`}); 
    var source_sql_resultset = snowflake.execute( {sqlText: `select SOURCE_NAME,
																	FREQUENCY,
																	coalesce ($$PRFL_$$||SOURCE_NAME||$$_$$||FREQUENCY,$$ $$)  AS GEN_TASK_NAME,
																	CASE WHEN FREQUENCY = $$DAY$$ THEN $$0 1 * * *$$
																	WHEN FREQUENCY = $$HOUR$$ THEN $$0 * * * *$$
																	ELSE $$0 1 * * *$$ END AS CRON_FREQ,
																	coalesce (ACT_TASK_NAME,$$ $$) as ACT_TASK_NAME,
																	TASK_STATE,
																	ACT_WAREHOUSE_NAME,
																	ACT_SCHEDULE,
																	coalesce ($$PRFL_$$||SOURCE_NAME||$$_$$||FREQUENCY||$$_STREAM$$,$$ $$)  AS GEN_STREAM_NAME,
																	$$CREATE STREAM if not exists $$||GEN_STREAM_NAME||$$ ON TABLE $$|| C.SOURCE_TBL_NM AS STREAM_DDL
																	from VW_PROFILE_SQL_BUILD_2 A FULL OUTER JOIN
																	TASK_TBL B
																	ON $$PRFL_$$||A.SOURCE_NAME||$$_$$||A.FREQUENCY = B.ACT_TASK_NAME
																	LEFT JOIN SOURCE_ODM_LOOKUP C 
																	ON A.SOURCE_NAME = C.ODM_NAME
																	GROUP BY 1,2,3,4,5,6,7,8,9,10`});     
     while (source_sql_resultset.next()) {
     var source_var =  source_sql_resultset.getColumnValue(1);
     var freq_var =  source_sql_resultset.getColumnValue(2);
     var gen_task_name_var =  source_sql_resultset.getColumnValue(3);
     var cron_var =  source_sql_resultset.getColumnValue(4);
     var act_task_var =  source_sql_resultset.getColumnValue(5);
     var act_task_st_var =  source_sql_resultset.getColumnValue(6);
	 var stream_name_var =  source_sql_resultset.getColumnValue(9);
	 var stream_ddl_var =  source_sql_resultset.getColumnValue(10);
     if (gen_task_name_var != '' '' && act_task_var == '' '') {
	    snowflake.execute({sqlText: stream_ddl_var});
     	var task_create_stmt = `CREATE OR REPLACE TASK ` + gen_task_name_var + `\\nWAREHOUSE = ` + WAREHOUSE + `\\nSCHEDULE = ''USING CRON ` + cron_var + ` UTC''\\n WHEN SYSTEM$STREAM_HAS_DATA($$` + stream_name_var + `$$) as call SP_INDEP_PROF_TRIGGER($$` + source_var + `$$,$$`+ freq_var +`$$);`;
        snowflake.execute({sqlText: task_create_stmt});
         snowflake.execute({sqlText: `ALTER TASK ` + gen_task_name_var + ` RESUME`});
                                                               }
    else if (gen_task_name_var != '' '' && act_task_var != '' '' && act_task_st_var == ''suspended'') {
    snowflake.execute({sqlText: `ALTER TASK ` + gen_task_name_var + ` RESUME`});     
                                                                                                     }
    else if (gen_task_name_var == '' '' && act_task_var != '' '') {
    snowflake.execute({sqlText: `DROP TASK ` + gen_task_name_var + `;`});     
                                                                                                     }                                                                                                 
                                            }                                     
                                                                                                     
  	snowflake.execute( {sqlText:''COMMIT''});
}
catch(err)
{
	snowflake.execute({sqlText: "ROLLBACK"});
	return err;
}
return "Succeeded";
';CREATE OR REPLACE PROCEDURE "SP_INDEP_PROF_TRIGGER"("SOURCE" VARCHAR(16777216), "FREQ" VARCHAR(16777216))
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
EXECUTE AS CALLER
AS '
try
{

    snowflake.execute( {sqlText:`BEGIN`});
    
    //TEMP TABLE CREATION FROM STREAM   
    var stream_tbl_resultset = snowflake.execute({sqlText: `Select STREAM_SQL from ANALYTICS.VW_PROFILE_SQL_BUILD_2 where SOURCE_NAME = $$` + SOURCE + `$$ AND FREQUENCY = $$` + FREQ + `$$;`});
    
     while (stream_tbl_resultset.next())
      {
	var strm_tm_table_sql = stream_tbl_resultset.getColumnValue(1);
      snowflake.execute({sqlText: strm_tm_table_sql});
      }
    
    //GET THE SQLS FOR SOURCE AND FREQUENCY
	var query_resultset = snowflake.execute({sqlText: `Select TEMP_TABLE,SQL from ANALYTICS.VW_PROFILE_SQL_BUILD_2 where SOURCE_NAME = $$` + SOURCE + `$$ AND FREQUENCY = $$` + FREQ + `$$;`});
	while (query_resultset.next())
    {
	var tm_table_nm = query_resultset.getColumnValue(1);
	var insert_sql_query = query_resultset.getColumnValue(2);
     
     //RUN THE SQLS FOR SOURCE,FREQ,CONTEXT AND POPULATE DATA TO TEMPORARY TABLE. RESULT WIH IN THE FORM OF JSON IN THIS STEP
    snowflake.execute({sqlText: insert_sql_query});
    
    //GET THE SQL FOR DECOUPLING THE JSON INTO SEPARATE ROWS
	var agg_query = snowflake.execute({sqlText: `Select SQL_QRY1,SQL_QRY2,TABLE_NM2 from VW_AGG_SQL_BUILD_2 where TABLE_NM = $$` + tm_table_nm + `$$;`});
	     while (agg_query.next())
		   {
		     var agg_sql_query = agg_query.getColumnValue(1);
    
    //SQL FOR CREATING TEMP TABLE WITH DECOUPLED ROWS. SEPARATE TEMP TABLE FOR ARRAY AND INTEGER      
		     snowflake.execute({sqlText: agg_sql_query});
             var insr_sql_query = agg_query.getColumnValue(2);
    
    //SQL FOR INSERTING DATA FROM TEMP TABLE TO PROFILE AGG.        
		     snowflake.execute({sqlText: insr_sql_query});
     
     //FETCHING TEMP TABLE NAME SO THAT IT CAN BE USED IN NOVELTY DETECTION           
             var tbl_nm2 = agg_query.getColumnValue(3);
            
       //STEP FOR GETTING ALL COMBINATIONS FOR HOURLY NOVELTY DETECTION INCLUDING VALUES
            var hr_novelty_meta =  snowflake.execute({sqlText: `Select distinct A.metric_id,A.batch_timestamp::string from `
              + tbl_nm2 + ` A JOIN PROFILE_COND2 B ON A.metric_id = B.ID
              JOIN PROFILE_AGG_DAYS_LOADED C ON A.metric_id = C.metric_id
             WHERE B.FREQUENCY = $$HOUR$$ AND NOVELTY_IND = $$Y$$`}); 
             
                 while (hr_novelty_meta.next())
		          {  
                    var met_id = hr_novelty_meta.getColumnValue(1);
                    var bt_timestamp = hr_novelty_meta.getColumnValue(2);
                    
      //RUN HOURLY NOVELY          
                    snowflake.execute({sqlText: `INSERT INTO NOVELTY_DETECTION
                                                 select A.*  from table(FN_PROFILE_TRANSFORM_TBL_HOUR(` + met_id + `,$$`+ bt_timestamp + `$$::TIMESTAMP_NTZ(9))) A LEFT JOIN table(FN_PROFILE_TRANSFORM_TBL_BT_HOUR(` + met_id + `,dateadd(hour, -2772, $$`+ bt_timestamp + 
                                                 `$$::TIMESTAMP_NTZ(9)),dateadd(hour, -1, $$`+ bt_timestamp + `$$::TIMESTAMP_NTZ(9)) )) B
                                                  ON A.VALUE = B.VALUE
                                                  AND A.CONTEXT_VALUE = B.CONTEXT_VALUE
                                                  LEFT JOIN NOVELTY_DETECTION C
                                                  ON A.METRIC_ID = C.METRIC_ID
                                                  AND A.BATCH_TIMESTAMP = C.BATCH_TIMESTAMP
                                                  AND A.CONTEXT_VALUE = C.CONTEXT_VALUE
                                                  AND A.VALUE = C.VALUE
                                                 JOIN PROFILE_PREV_DAY_LOADED D
                                                  ON A.METRIC_ID = D.METRIC_ID
                                                  AND A.BATCH_TIMESTAMP = D.BATCH_TIMESTAMP
                                                  AND A.CONTEXT_VALUE = D.CONTEXT_VALUE
                                                  WHERE  B.VALUE IS NULL
                                                  AND  C.VALUE IS NULL
                                                  AND D.PREV_DAY_COUNT > 14;`
                                      }); 
		          }	  
           
           
         //STEP FOR GETTING ALL COMBINATIONS FOR DAILY NOVELTY DETECTION INCLUDING VALUES  
              var dly_novelty_meta =  snowflake.execute({sqlText: `Select distinct A.metric_id,A.batch_timestamp::date::string from `
              + tbl_nm2 + ` A JOIN PROFILE_COND2 B ON A.metric_id = B.ID
              JOIN PROFILE_AGG_DAYS_LOADED C ON A.metric_id = C.metric_id
             WHERE B.FREQUENCY = $$DAY$$ AND NOVELTY_IND = $$Y$$`}); 
             
                 while (dly_novelty_meta.next())
		          {  
                    var met_id = dly_novelty_meta.getColumnValue(1);
                    var bt_timestamp = dly_novelty_meta.getColumnValue(2);
                    snowflake.execute({sqlText: `INSERT INTO NOVELTY_DETECTION
                                                 select A.*  from table(FN_PROFILE_TRANSFORM_TBL_DATE(` + met_id + `,$$`+ bt_timestamp + `$$::date)) A LEFT JOIN table(FN_PROFILE_TRANSFORM_TBL_BETWEENDATES(` + met_id + `,dateadd(day, -90, $$`+ bt_timestamp + 
                                                 `$$::date),dateadd(day, -1, $$`+ bt_timestamp + `$$::date) )) B
                                                  ON A.VALUE = B.VALUE
                                                  AND A.CONTEXT_VALUE = B.CONTEXT_VALUE
                                                  LEFT JOIN NOVELTY_DETECTION C
                                                  ON A.METRIC_ID = C.METRIC_ID
                                                  AND A.BATCH_TIMESTAMP = C.BATCH_TIMESTAMP
                                                  AND A.CONTEXT_VALUE = C.CONTEXT_VALUE
                                                  AND A.VALUE = C.VALUE
                                                  JOIN PROFILE_PREV_DAY_LOADED D
                                                  ON A.METRIC_ID = D.METRIC_ID
                                                  AND A.BATCH_TIMESTAMP = D.BATCH_TIMESTAMP
                                                  AND A.CONTEXT_VALUE = D.CONTEXT_VALUE
                                                  WHERE  B.VALUE IS NULL
                                                  AND  C.VALUE IS NULL
                                                  AND D.PREV_DAY_COUNT > 14 ;`
                                      }); 
		          }	  
                  
             //CUSTOMER ALERTS     
             snowflake.execute({sqlText: `INSERT INTO CUSTOM_ALERTS
             SELECT A.* FROM 
             (Select distinct 
             A.METRIC_DEFINITION,
             A.METRIC_ID,
             A.BATCH_TIMESTAMP,
             A.CONTEXT_KEY,
             A.CONTEXT_VALUE,
             lfo.value::VARCHAR value,
             uuid_string() AS GUID,
             current_timestamp::TIMESTAMP_NTZ(9) AS COMMIT_TIME,
             A.ACCOUNT_ID
             from `
              + tbl_nm2 + ` A , lateral flatten(input=>A.METRIC_VALUE) lfo) A
              JOIN PROFILE_COND2 B ON A.metric_id = B.ID
              WHERE  ALERT_IND = $$Y$$`
                               });      
           }
           
	snowflake.execute({sqlText: `DROP TABLE ` + tm_table_nm + `;`}); 
    
    }
    
	snowflake.execute( {sqlText:`COMMIT`});
}
catch(err)
{
	snowflake.execute({sqlText: "ROLLBACK"});
	return err;
}
return "Succeeded";
';


CREATE OR REPLACE FUNCTION "FN_PROFILE_TRANSFORM_TBL_HOUR"("PRFL_ID" NUMBER(38,0), "TS" TIMESTAMP_NTZ(9))
RETURNS TABLE ("METRIC_DEFINITION" VARCHAR(16777216), "METRIC_ID" NUMBER(38,0), "BATCH_TIMESTAMP" TIMESTAMP_NTZ(9), "CONTEXT_KEY" VARCHAR(16777216), "CONTEXT_VALUE" VARCHAR(16777216), "VALUE" VARCHAR(16777216), "GUID" VARCHAR(16777216), "COMMIT_TIME" TIMESTAMP_NTZ(9), "ACCOUNT_ID" VARCHAR(16777216))
LANGUAGE SQL
AS ' SELECT DISTINCT METRIC_DEFINITION,METRIC_ID,PROFILE_AGG.BATCH_TIMESTAMP,PROFILE_AGG.CONTEXT_KEY,PROFILE_AGG.CONTEXT_VALUE, lfo.value::VARCHAR value,
uuid_string() AS GUID,
current_timestamp::TIMESTAMP_NTZ(9) AS COMMIT_TIME,
PROFILE_AGG.ACCOUNT_ID
FROM ANALYTICS.PROFILE_AGG AS PROFILE_AGG, lateral flatten(input=>PROFILE_AGG.METRIC_VALUE) lfo
WHERE PROFILE_AGG.BATCH_TIMESTAMP = TS
  AND PROFILE_AGG.METRIC_ID = PRFL_ID
';



CREATE OR REPLACE FUNCTION "FN_PROFILE_TRANSFORM_TBL_BT_HOUR"("PRFL_ID" NUMBER(38,0), "ST_TS" TIMESTAMP_NTZ(9), "END_TS" TIMESTAMP_NTZ(9))
RETURNS TABLE ("METRIC_ID" NUMBER(38,0), "CONTEXT_KEY" VARCHAR(16777216), "CONTEXT_VALUE" VARCHAR(16777216), "VALUE" VARCHAR(16777216))
LANGUAGE SQL
AS ' SELECT DISTINCT 
METRIC_ID,
PROFILE_AGG.CONTEXT_KEY,
PROFILE_AGG.CONTEXT_VALUE, 
lfo.value::VARCHAR value
FROM ANALYTICS.PROFILE_AGG AS PROFILE_AGG, lateral flatten(input=>PROFILE_AGG.METRIC_VALUE) lfo
WHERE PROFILE_AGG.BATCH_TIMESTAMP BETWEEN ST_TS and END_TS
  AND PROFILE_AGG.METRIC_ID = PRFL_ID
';



CREATE OR REPLACE FUNCTION "FN_PROFILE_TRANSFORM_TBL_DATE"("PRFL_ID" NUMBER(38,0), "TS" DATE)
RETURNS TABLE ("METRIC_DEFINITION" VARCHAR(16777216), "METRIC_ID" NUMBER(38,0), "BATCH_TIMESTAMP" TIMESTAMP_NTZ(9), "CONTEXT_KEY" VARCHAR(16777216), "CONTEXT_VALUE" VARCHAR(16777216), "VALUE" VARCHAR(16777216), "GUID" VARCHAR(16777216), "COMMIT_TIME" TIMESTAMP_NTZ(9), "ACCOUNT_ID" VARCHAR(16777216))
LANGUAGE SQL
AS ' SELECT DISTINCT METRIC_DEFINITION,METRIC_ID,PROFILE_AGG.BATCH_TIMESTAMP,PROFILE_AGG.CONTEXT_KEY,PROFILE_AGG.CONTEXT_VALUE, lfo.value::VARCHAR value,
uuid_string() AS GUID,
current_timestamp::TIMESTAMP_NTZ(9) AS COMMIT_TIME,
PROFILE_AGG.ACCOUNT_ID
FROM ANALYTICS.PROFILE_AGG AS PROFILE_AGG, lateral flatten(input=>PROFILE_AGG.METRIC_VALUE) lfo
WHERE PROFILE_AGG.BATCH_TIMESTAMP::DATE = TS
  AND PROFILE_AGG.METRIC_ID = PRFL_ID
';


CREATE OR REPLACE FUNCTION "FN_PROFILE_TRANSFORM_TBL_BETWEENDATES"("PRFL_ID" NUMBER(38,0), "TS1" DATE, "TS2" DATE)
RETURNS TABLE ("METRIC_DEFINITION" VARCHAR(16777216), "METRIC_ID" NUMBER(38,0), "CONTEXT_KEY" VARCHAR(16777216), "CONTEXT_VALUE" VARCHAR(16777216), "VALUE" VARCHAR(16777216))
LANGUAGE SQL
AS ' 

SELECT DISTINCT A.METRIC_DEFINITION,A.METRIC_ID,A.CONTEXT_KEY,A.CONTEXT_VALUE, lfo.value::VARCHAR value
FROM ANALYTICS.PROFILE_AGG A, lateral flatten(input=>A.METRIC_VALUE) lfo
WHERE A.BATCH_TIMESTAMP::DATE BETWEEN TS1 and TS2
  AND A.METRIC_ID = PRFL_ID
';


CREATE OR REPLACE FUNCTION "ARRAY_FLAT"("A" ARRAY)
RETURNS ARRAY
LANGUAGE JAVASCRIPT
AS '
const arr1 = A;

const arr3 = arr1.flat();

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

var unique = arr3.filter(onlyUnique);

return(unique);

';


create or replace task TASK_BUILDER_PRFL
	warehouse={{ warehouse }}
	schedule='USING CRON 30 8 * * * UTC'
	as call SP_PRFL_TASK_BUILDER('?WH?');
