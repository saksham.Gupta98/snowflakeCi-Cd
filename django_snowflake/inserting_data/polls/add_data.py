
from rest_framework.views import APIView
from django.http import JsonResponse
import os
from inserting_data.libs.add_data_logic import snowflake_row_logic

class add_rows_api(APIView):
    def post(self, request):
        json_input = request.data
        print(json_input)
        try:
            conn = snowflake_row_logic()
            test = conn.add_logic(json_input)
            return JsonResponse ({'success': True, 'message':test})
            #return JsonResponse (test)
        except Exception as e:
            print(str(e))
            return JsonResponse({'success':False, 'message': str(e)}, status = 500)

