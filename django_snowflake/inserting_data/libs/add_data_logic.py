
from rest_framework import exceptions
from rest_framework.views import APIView
import snowflake.connector
import requests
import json
from ntpath import join
import pandas 
from snowflake.connector.pandas_tools import write_pandas
from snowflake.connector.pandas_tools import pd_writer


#function to create cursor and table and add rows
def snowflake_logic(dic):
    conn = snowflake.connector.connect(
    user = 'NAREN',
    password = 'Naren1997',
    account = 'elysiumpartner',
    role = 'ANALYST',
    warehouse = 'DEMO_WH',
    database = 'TENANT3',
    schema = 'API_TEST'
)

    cur = conn.cursor()

   #getting the data into two lists (column names and row 1)
    t1 = dic
    keys = []
    values = []
        
    for x,y in dic.items():
        keys.append(x)
        values.append(y)

    keys_up = [a.upper() for a in keys]

    row1 = (tuple(values))

    #creating table in snowflake
    create_table = ('''CREATE TABLE test_table_1 IF NOT EXISTS (
    client_id varchar(100),
    account_name varchar(100),
    username varchar(100),
    password varchar(100), 
    role varchar(100),
    warehouse varchar(100),
    database varchar(100),
    schema varchar(100),
    component_name varchar(400), 
    parameters varchar(400),
    commit_time varchar(100)
    )
    ''')

    cur.execute(create_table)

    #retrieving timestamp from snowflake
    time1 = cur.execute('select current_timestamp').fetchone()

    df = pandas.DataFrame([row1], columns= keys_up)
    df['COMMIT_TIME'] = time1
    success, nchunks, nrows, _ = write_pandas(conn, df, "TEST_TABLE_1")
    print(df)
    return 'row inserted in table'
   
#getting the data from postman and passing the main logic function (linked to the APIView/add_data file)
class snowflake_row_logic(APIView):
    def add_logic(self, json_input):
        
        dic = json_input 
        print('')
        return snowflake_logic(dic)
        
        

