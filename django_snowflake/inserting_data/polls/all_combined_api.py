from rest_framework.views import APIView
from django.http import JsonResponse
import os
from inserting_data.libs.all_combined_logic import ultimate_logic

class all_logic(APIView):
    def post(self, request):
        json_input = request.data
        print(json_input)
        try:
            conn = ultimate_logic()
            test = conn.logic_function(json_input)
            return JsonResponse ({'success': True, 'message':test})
        except Exception as e:
            print(str(e))
            return JsonResponse({'success':False, 'message': str(e)}, status = 500)